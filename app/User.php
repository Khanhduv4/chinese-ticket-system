<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
	use Notifiable;

	protected $table = 'users';
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'email', 'password',
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	/**
	 * The attributes that should be cast to native types.
	 *
	 * @var array
	 */
	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	protected $appends = ['total_guest', 'collected_ticket', 'vip_ticket', 'ticket_left', 'city'];

	public function getTotalGuestAttribute()
	{
		return Customer::where("user_id", $this->id)->count();
	}

	public function getTicketLeftAttribute()
	{
		return Ticket::where("user_id", $this->id)->where("guest_id", null)->count();
	}

	public function getCollectedTicketAttribute()
	{
		return Customer::where("user_id", $this->id)->where("ticket_status", Constant::TICKET_STATUS_COLLECTED)->count();
	}

	public function getVipTicketAttribute()
	{
		return Customer::where("user_id", $this->id)->where("ticket_type", Constant::TICKET_TYPE_VIP)->count();
	}

	public function getScoreFromPostAttribute()
	{
		$posts = Post::where("posted_by", $this->id)->where("status", Constant::POST_STATUS_APPROVED)->get();
		$score = 0;
		foreach ($posts as $post) {
			$score += $post->score;
		}
		return $score;
    }

	public function get($id = null)
	{
		if ($id) {
			return self::find($id);
		} else {
			return self::all();
		}
	}

	public function getAffiliater()
	{
		return self::where("role", Constant::USER_ROLE_AFFILIATER)->get();
    }

	public function getRanked()
	{
        $users = self::where("role", Constant::USER_ROLE_AFFILIATER)->get();
        foreach ($users as $user) {
            $user->total_score = $user->score + $user->scoreFromPost;
            $user->save();
        }
        $users = self::where("role", Constant::USER_ROLE_AFFILIATER)->orderBy('total_score', 'desc')->get();

		return $users;
	}

	public function getOrganization()
	{
		return self::groupBy("organization")->having("role", Constant::USER_ROLE_AFFILIATER)->get();
	}

	public function getCityAttribute()
	{
		$cityModel = new City;
		$city = $cityModel->get($this->city_id);

		return $city;
	}

	public function totalGuest()
	{
		return Customer::count();
    }

    public function getByPhone($phone) {
        return self::where('phone', $phone)->get();
    }
}
