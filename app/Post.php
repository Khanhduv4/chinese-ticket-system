<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }
    public function getByUserId($id)
    {
        return self::where('posted_by', $id)->get();
    }
    public function user()
    {
        return $this->belongsTo('App\User', "posted_by");
    }
}
