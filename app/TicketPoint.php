<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketPoint extends Model
{
    protected $table = 'ticket_points';

    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }
    public function getByUserId($id)
    {
        return self::where('user_id', $id)->get();
    }

    public function user()
    {
        return $this->belongsTo('App\User', "user_id");
    }
}
