<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Ticket;
use App\User;
use App\City;
use App\Constant;


class TicketController extends Controller
{

    private $model;
    private $userModel;
    private $ticketPointModel;
    private $cityModel;



    public function __construct()
    {
        $this->model = new Ticket;
        $this->userModel = new User;
        $this->cityModel = new City;
    }

    public function view()
    {
        if (Auth::user()->role == 0) {
            $data['tickets'] = $this->model->get();
            $data['users'] = $this->userModel->getAffiliater();
        }
        return view("admin.managements.tickets", $data);
    }

    public function getById(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $ticket = $this->model->get($req->input('id'));
        if ($ticket)
            return response(["status" => "OK", "ticket" => $ticket], 200);
        return 0;
    }

    public function getTicketPoints()
    {
        if (!Auth::check())
            return 0;
        else {
            if (Auth::user()->role == 0) {
                return $this->ticketPointModel->get();
            } else {
                return $this->ticketPointModel->getByUserId(Auth::user()->id);
            }
        }
    }

    public function create(Request $req)
    {
        if (!isset($req)) return response("Request is invalid !", 400);


        $start = $req->input('start');
        $end = $req->input('end');

        $duplicatedCode = [];



        for ($i = $start; $i <= $end; $i++) {

            if ($this->model->getByCode($i)) {
                array_push($duplicatedCode, $i);
                continue;
            }

            $ticket = new Ticket();

            $ticket->code = $i;

            if (strpos($ticket->code, "4")) continue;


            $ticket->user_id = $req->input("user");

            $ticket->save();
        }


        return response(['duplicated' => $duplicatedCode], 200);
    }

    public function update(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $ticket = Ticket::find($req->input('id'));
        $ticket->code = $req->input('code');
        $ticket->user_id = $req->input('user');
        $ticket->save();
        return response("OK", 200);
    }

    public function giveTicket(Request $req)
    {
        if ($req->input('id') == NULL) return response("Id is null !", 400);
        $customer = Customer::find($req->input('id'));
        $this->checkVIP($customer->ref_id);
        $customer->ticket_status = 1;
        $customer->save();
        return response("OK", 200);
    }


    public function delete(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $ticket = Ticket::find($req->input('id'));
        if ($ticket)
            $ticket->delete();
        return response("OK", 200);
    }
}
