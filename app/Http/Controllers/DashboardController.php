<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use App\Customer;
use App\TicketPoint;
use App\Constant;


class DashboardController extends Controller
{

    private $userModel;
    private $customerModel;


    public function __construct()
    {
        $this->userModel = new User;
        $this->customerModel = new Customer;
    }

    public function view()
    {
        $data["users"] = $this->userModel->getAffiliater();
        $data["ticketPoints"] = $this->userModel->getAffiliater()->count();
        $data["vipGuest"] = $this->customerModel->getByTicketType(Constant::TICKET_TYPE_VIP)->count();
        $data["collectedTicket"] = $this->customerModel->getByTicketStatus(Constant::TICKET_STATUS_COLLECTED)->count();
        $data["totalGuest"] = $this->customerModel->get()->count();

        return view("admin.managements.dashboard", $data);
    }
}
