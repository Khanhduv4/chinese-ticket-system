<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use App\City;
use App\Constant;
use App\Ticket;


class UserController extends Controller
{

    private $model;
    private $cityModel;
    private $ticketModel;


    public function __construct()
    {
        $this->model = new User;
        $this->cityModel = new City;
        $this->ticketModel = new Ticket;
    }

    public function view()
    {
        $data["users"] = $this->model->get();
        $data["cities"] = $this->cityModel->get();
        return view("admin.managements.users", $data);
    }

    public function viewRanked() {
        $data["ranked"] = $this->model->getRanked();
        return view("admin.managements.ranked", $data);
    }

    public function getById(Request $req)
    {
//	dd($req->fullUrl());
	if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $user = $this->model->get($req->input('id'));
        if ($user)
            return response(["status" => "OK", "user" => $user], 200);
    }

    public function create(Request $req)
    {
        if (!isset($req)) return response("Request is invalid !", 400);

        $user = new User();
        $user->phone = $req->input("phone");
        $user->password = Hash::make($req->input("password"));
        $user->name = $req->input("name");
        $user->code = $req->input("code");
        $user->role = $req->input("role");
        $user->address = $req->input('address');

        $city = City::find($req->input('city'));


        if (!$city) {
            $city = new City();
            $city->name = $req->input('newCity');
            $city->save();
        }

        $user->city_id = $city->id;

        $user->organization = $req->input('organization');
        $user->save();
        return response("OK", 200);
    }
    public function update(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $user = User::find($req->input('id'));
        $user->name = $req->input("name");
        $user->phone = $req->input("phone");
        $user->code = $req->input("code");
        $user->address = $req->input('address');

        $city = City::find($req->input('city'));


        if (!$city) {
            $city = new City();
            $city->name = $req->input('newCity');
            $city->save();
        }

        $user->city_id = $city->id;
        $user->organization = $req->input('organization');
        $user->role = $req->input("role");
        $user->save();
        return response("OK", 200);
    }

    public function changePassword(Request $req)
    {

        if (!Auth::check()) return 0;
        if ($req->input('password') == NULL) return response("password is invalid !", 400);
        $newPassword = $req->input('password');
        $user = User::find(Auth::user()->id);
        $user->password = Hash::make($newPassword);
        $user->save();
        return response("OK", 200);
    }

    public function importXls(Request $req)
    {
        $users = $req->input('json');
        foreach ($users as $user) {
            if ($this->model->getByPhone($user['phone'])) continue;

            $city = $this->cityModel->GetByName($user['city']);

            if (!$city) {
                $city = new City();
                $city->name = $user['city'];
                $city->save();
            }

            $newUser = new User();
            $newUser->city_id = $city->id;
            $newUser->name = $user['name'];
            $newUser->phone = $user['phone'];
            $newUser->code = $user['code'];
            $newUser->organization = $user['organization'];
            $newUser->role = Constant::USER_DEFAULT_ROLE;
            $newUser->password = Hash::make(Constant::USER_DEFAULT_PASSWORD);
            $newUser->address = $user['address'];

            $newUser->save();
            for ($i = $user['start_ticket_code']; $i < $user['end_ticket_code']; $i++) {
                $newTicket = new Ticket();
                $newTicket->code = $i;
                if (strpos($newTicket->code, "4")) continue;
                $newTicket->user_id = $newUser['id'];
                $newTicket->save();
            }
        }
        return response("OK", 200);
    }

    public function getTicketLeft(Request $req)
    {
        $tickets = Ticket::where('user_id', Auth::user()->id)->where('guest_id', null)->get();
        return response()->json($tickets, 200);
    }

    public function delete(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $user = User::find($req->input('id'));
        if ($user)
            $user->delete();
        return response("OK", 200);
    }
}
