<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;


class PostController extends Controller
{

    private $model;


    public function __construct()
    {
        $this->model = new Post;
    }
    public function view()
    {
        if (Auth::user()->role == 0)
            $data['posts'] = $this->model->get();
        else
            $data["posts"] = $this->model->getByUserId(Auth::user()->id);
        return view("admin.managements.posts", $data);
    }
    public function viewCreator($id = null)
    {
        if ($id) {
            $data["method"] = "update";
            $post = $this->model->get($id);
            if (Auth::user()->id == $post->posted_by) {
                $data["post"] = $post;
            } else {
                return view('errors.403');
            }

            return view("admin.managements.post-editor", $data);
        } else {
            $data["method"] = "create";
            return view("admin.managements.post-editor", $data);
        }
    }
    public function getById(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $post = $this->model->get($req->input('id'));
        if ($post)
            return response(["status" => "OK", "post" => $post], 200);
    }
    public function create(Request $req)
    {
        if (!isset($req)) return response("Request is invalid !", 400);

        $post = new Post();
        $post->news_feed = $req->input("news_feed");
        $post->media = $req->input("media");
        $post->website = $req->input("website");
        $post->score = $this->score($req->input("news_feed"), $req->input("media"), $req->input("website"));
        $post->body = $req->input("body");
        $post->posted_by = Auth::user()->id;
        $post->save();
        return response("OK", 200);
    }
    public function update(Request $req)
    {
        if ($req->input('id') == NULL) return response("Id is null !", 400);
        $post = Post::find($req->input('id'));
        if ($post->posted_by != Auth::user()->id) return response("You don't have permission !", 403);
        $post->news_feed = $req->input("news_feed");
        $post->media = $req->input("media");
        $post->website = $req->input("website");
        $post->score = $this->score($req->input("news_feed"), $req->input("media"), $req->input("website"));
        $post->body = $req->input("body");

        $post->save();
        return response("OK", 200);
    }
    public function review(Request $req)
    {
        if ($req->input('id') == NULL) return response("Id is null !", 400);
        $post = Post::find($req->input('id'));
        $post->message = $req->input("message");
        $post->status = $req->input("status");
        $post->save();
        return response("OK", 200);
    }
    public function delete(Request $req)
    {
        if ($req->input('id') != Auth::user()->id) return response("You don't have permission !", 403);

        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $post = Post::find($req->input('id'));
        if ($post)
            $post->delete();
        return response("OK", 200);
    }

    public function score($newsFeed, $media, $website)
    {
        return $newsFeed * 1 + $media * 3 + $website * 5;
    }
}
