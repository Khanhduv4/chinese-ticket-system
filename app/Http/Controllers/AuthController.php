<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }
    public function view()
    {
        if (Auth::check()) {
            return redirect()->intended('/admin/posts');
        }
        return view('admin.auth.login');
    }
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $req)
    {
        $arr = [
            'phone' => $req->input("phone"),
            'password' => $req->input("password"),
        ];
        if (Auth::attempt($arr)) {
            return response()->json(1, 200);
        } else {
            return response()->json(0, 200);
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
