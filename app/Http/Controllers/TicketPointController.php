<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TicketPoint;


class TicketPointController extends Controller
{

    private $model;


    public function __construct()
    {
        $this->model = new TicketPoint;
    }

    public function view()
    {
        if (Auth::user()->role == 0)
            $data['points'] = $this->model->get();
        else
            $data["points"] = $this->model->getByUserId(Auth::user()->id);
        return view("admin.managements.ticket-points", $data);
    }

    public function getById(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $point = $this->model->get($req->input('id'));
        if ($point)
            return response(["status" => "OK", "point" => $point], 200);
    }


    public function create(Request $req)
    {
        if (!isset($req)) return response("Request is invalid !", 400);

        $point = new TicketPoint();
        $point->city = $req->input("city");
        $point->address = $req->input("address");
        // $point->prefix = $req->input("prefix");
        if ($this->model->where("prefix", $point->prefix)->first())
            return response("Prefix is exists !", 400);
        $point->user_id = Auth::user()->id;
        $point->save();
        return response("OK", 200);
    }
    public function update(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $point = TicketPoint::find($req->input('id'));
        $point->city = $req->input("city");
        $point->address = $req->input("address");
        $point->prefix = $req->input("prefix");
        $point->save();
        return response("OK", 200);
    }
    public function delete(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $point = TicketPoint::find($req->input('id'));
        if ($point)
            $point->delete();
        return response("OK", 200);
    }
}
