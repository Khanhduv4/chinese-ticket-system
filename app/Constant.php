<?php

namespace App;


class Constant
{
    const POST_STATUS_PENDING = 0;
    const POST_STATUS_APPROVED = 1;
    const POST_STATUS_DISAPPROVED = 2;

    const TICKET_TYPE_NORMAL = 0;
    const TICKET_TYPE_VIP = 1;

    const TICKET_STATUS_REGISTERED = 0;
    const TICKET_STATUS_COLLECTED = 1;

    const USER_DEFAULT_PASSWORD = "123456";
    const USER_DEFAULT_ROLE = 1;

    const USER_ROLE_ADMIN = 0;
    const USER_ROLE_AFFILIATER = 1;
}
