<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Constant;

class Customer extends Model
{
	protected $appends = ['rank', "refs_count", "collected_ticket"];

	public function getRankAttribute()
	{
        $customers = Customer::orderBy("score", "desc")->get();

        $rank = 0;

        foreach ($customers as $key => $customer) {
            if ($customer->id == $this->id) {
                $rank = $key + 1;
                break;
            }
        }
        return $rank;
	}

	public function getRefsCountAttribute()
	{
		return Customer::where('ref_id', $this->id)->count();
	}
	public function getCollectedTicketAttribute()
	{
		return Customer::where('ref_id', $this->id)->where('ticket_status', Constant::TICKET_STATUS_COLLECTED)->count();
	}
	public function get($id = null)
	{
		if ($id) {
			return self::find($id);
		} else {
			return self::all();
		}
	}
	public function getByPhone($phone)
	{
		return self::where('phone', $phone)->with('user')->first();
	}
	public function getByUserId($id)
	{
		return self::where('user_id', $id)->get();
	}

	public function getByRefCode($refCode)
	{
		return self::where('ref_code', $refCode)->first();
	}

	public function countRefs($guest_id)
	{
		return self::where('ref_id', $guest_id)->where("ticket_status", Constant::TICKET_STATUS_COLLECTED)->count();
	}

	public function getRefs($ref_id)
	{
		return self::where('ref_id', $ref_id)->get();
    }

    public function getRanked()
	{
        $users = self::where("role", Constant::USER_ROLE_AFFILIATER)->get();
        foreach ($users as $user) {
            $user->total_score = $user->score + $user->scoreFromPost;
            $user->save();
        }
        $users = self::where("role", Constant::USER_ROLE_AFFILIATER)->orderBy('total_score', 'desc')->get();

		return $users;
    }

    public function getTicket($guest_id) {
        return Ticket::where('guest_id', $guest_id)->first();
    }

	public function getTop100()
	{
		return DB::select('select id,name,refs from customers c inner join (select count(ref_id) as refs, ref_id from customers group by (ref_id)) t on t.ref_id=c.id order by refs DESC');
	}

	public function getByTicketType($ticketType)
	{
		return self::where('ticket_type', $ticketType)->get();
	}

	public function getByTicketStatus($ticketStatus)
	{
		return self::where('ticket_status', $ticketStatus)->get();
	}

	public function user()
	{
		return $this->belongsTo('App\User', "user_id");
	}
}
