<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';

    public function get($id = null)
    {
        if ($id) {
            return self::find($id);
        } else {
            return self::all();
        }
    }

    public function getByCode($code)
    {
        return self::where('code', $code)->first();
    }

    public function user()
    {
        return $this->belongsTo('App\User', "user_id");
    }

    public function getAvailableTicket($user_id)
    {
        return self::where("user_id", $user_id)->where("guest_id", null)->first();
    }
}
