<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class City extends Model
{
	protected $table = 'cities';

	public function GetByName($name)
	{
		return self::where('name', $name)->first();
	}
	public function get($id = null)
	{
		if ($id) {
			return self::find($id);
		} else {
			return self::all();
		}
	}
}
