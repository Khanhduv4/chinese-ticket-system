@extends("admin.layouts.master")

@section("content")

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Dashboard</h3>
	</div>
	<div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div>
	<div>
		<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
	</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Row -->
	<!-- Row -->
	<div class="row">
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<!-- Row -->
					<div class="row">
						<div class="col-8"><h2>{{$totalGuest ?? 0}}</h2>
							<h6>Registered guests</h6></div>
						<div class="col-4 align-self-center text-right  p-l-0">
							<div id="sparklinedash3"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<!-- Row -->
					<div class="row">
						<div class="col-8"><h2 class="">{{$collectedTicket ?? 0}} <i class="ti-angle-up font-14 text-success"></i></h2>
							<h6>Collected tickets</h6></div>
						<div class="col-4 align-self-center text-right p-l-0">
							<div id="sparklinedash"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<!-- Row -->
					<div class="row">
						<div class="col-8"><h2>{{$vipGuest ?? 0}} <i class="ti-angle-up font-14 text-success"></i></h2>
							<h6>VIP Tickets</h6></div>
						<div class="col-4 align-self-center text-right p-l-0">
							<div id="sparklinedash2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<!-- Row -->
					<div class="row">
						<div class="col-8"><h2>{{$ticketPoints}} <i class="ti-angle-down font-14 text-danger"></i></h2>
							<h6>Ticket Points</h6></div>
						<div class="col-4 align-self-center text-right p-l-0">
							<div id="sparklinedash4"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Row -->
	{{-- <div class="row">
		<div class="col-lg-4 col-xlg-3">
			<div class="card card-inverse card-info">
				<div class="card-body">
					<div class="d-flex">
						<div class="m-r-20 align-self-center">
							<h1 class="text-white"><i class="ti-user"></i></h1></div>
						<div>
							<h3 class="card-title">Total guests</h3>
							<h6 class="card-subtitle">Registered guests</h6> </div>
					</div>
					<div class="row">
						<div class="col-4 align-self-center">
							<h2 class="font-light text-white">{{$totalGuest ?? 0}}</h2>
						</div>
						<div class="col-8 p-t-10 p-b-20 text-right">
							<div class="spark-count" style="height:65px"></div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-lg-4 col-xlg-3">

			<div class="card card-inverse card-danger">
				<div class="card-body">
					<div class="d-flex">
						<div class="m-r-20 align-self-center">
							<h1 class="text-white"><i class="ti-ticket"></i></h1></div>
						<div>
							<h3 class="card-title">Collected tickets</h3>
							<h6 class="card-subtitle">
								The number of tickets taken</h6> </div>
					</div>
					<div class="row">
						<div class="col-4 align-self-center">
							<h2 class="font-light text-white">{{$collectedTicket ?? 0}}</h2>
						</div>
						<div class="col-8 p-t-10 p-b-20 text-right align-self-center">
							<div class="spark-count2" style="height:65px"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-xlg-3">
			<div class="card card-inverse card-warning">
				<div class="card-body">
					<div class="d-flex">
						<div class="m-r-20 align-self-center">
							<h1 class="text-white"><i class="ti-crown"></i></h1></div>
						<div>
							<h3 class="card-title">Total VIP</h3>
							<h6 class="card-subtitle">
								The number of VIP tickets</h6> </div>
					</div>
					<div class="row">
						<div class="col-4 align-self-center">
							<h2 class="font-light text-white">{{$vipGuest ?? 0}}</h2>
						</div>
						<div class="col-8 p-t-10 p-b-20 text-right">
							<div class="spark-count" style="height:65px"></div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-lg-4 col-xlg-3">

			<div class="card card-inverse card-primary">
				<div class="card-body">
					<div class="d-flex">
						<div class="m-r-20 align-self-center">
							<h1 class="text-white"><i class="ti-bookmark-alt"></i></h1></div>
						<div>
							<h3 class="card-title">Total ticket points</h3>
							<h6 class="card-subtitle">
								The number of ticket points</h6> </div>
					</div>
					<div class="row">
						<div class="col-4 align-self-center">
						<h2 class="font-light text-white">{{$ticketPoints}}</h2>
						</div>
						<div class="col-8 p-t-10 p-b-20 text-right align-self-center">
							<div class="spark-count2" style="height:65px"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> --}}
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">User statistics</h4>
			<h6 class="card-subtitle">User statistics table</h6>
			<div class="table-responsive m-t-40">
				<table id="myTable" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>Organization</th>
							<th>Available Ticket</th>
							<th>Collected tickets</th>
							<th>VIP Tickets</th>
							<th>Total Score</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($users as $user)
						<tr>
							<td>{{$user->name}}</td>
							<td>{{$user->organization}}</td>
							<td>{{$user->ticket_left}}</td>
							<td>{{$user->collected_ticket}}</td>
							<td>{{$user->vip_ticket}}</td>
							<td>{{$user->score}}</td>
						</tr>
						@endforeach

					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Row -->
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

@endsection

@section('scripts')
<!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->

    <script src="{{asset("admin/plugins/sparkline/jquery.sparkline.min.js")}}"></script>

    <script src="{{asset("admin/js/dashboard4.js")}}"></script>


	
    <script src="{{asset("admin/js/dashboard2.js")}}"></script>
@endsection