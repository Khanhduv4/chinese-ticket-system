@extends('admin.layouts.master')

@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Ticket Points Management</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
				<li class="breadcrumb-item active">Ticket Points</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row p-3">
							<div class="col-md-8">
								<h3 class="card-title">Ticket Points List</h3>
							</div>
							@if ($points->count() <= 0)
							<div class="col-md-4">
								<button type="button" class="btn waves-effect waves-light btn-primary pull-right" data-toggle="modal" data-target="#infoModal">Create Ticket Point</button>

							</div>
							@endif


						</div>
						<div class="table-responsive p-1">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th class = "text-center">#</th>
										<th>City</th>
										<th class = "text-right">Address</th>
										@if(Auth::user()->role == 0)
										<th class = "text-right">Created By</th>
										@endif
										<th class = "text-right">Action</th>
										<th class = "text-right">Created At</th>
										<th class = "text-right">Update At</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($points as $point)
									<tr>
										<td class = "text-center">{{$loop->index + 1}}</td>
										<td>
											{{$point->city}}
										</td>
										<td class = "text-right">
											{{$point->address}}
										</td>
										@if(Auth::user()->role == 0)
										<td data-html=true data-toggle="tooltip" title="Name: {{$point->user->name}} <br/>Phone: {{$point->user->phone}}" class = "text-right">
											{{$point->user->name}}
										</td>
										@endif
										<td class = "text-right point-action">
											<a href="" data-id = {{$point->id}} class = "editBtn" data-toggle="modal" data-target="#infoModal"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
										</td>
										<td class = "text-right">
											{{date('H:i:s d/m/Y', strtotime($point->created_at))}}
										</td>
										<td class = "text-right">
											{{date('H:i:s d/m/Y', strtotime($point->updated_at))}}
										</td>
						
									</tr>
									@endforeach

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->
	<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
		<div class="modal-dialog" role="document">
			<div class="preloader">
				<svg class="circular" viewbox="25 25 50 50">
					<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
			</div>
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title" id="exampleModalLabel1">Ticket Point Info</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<form id = "infoForm" novalidate>
						<div class="form-group">
							<label for="city" class="control-label">City</label>
							<input type="text" required placeholder="Example: Beijing" class="form-control" name = "city" />
							<div class="help-block"></div>
						</div>
						<div class="form-group">
							<label for="address" class="control-label">Address</label>
							<input type="text" required placeholder="Example: 123 Tran Hung Dao street" class="form-control" name="address" />
							<div class="help-block"></div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" form = "infoForm"  data-function = "create" id = "saveBtn" class="btn btn-primary">Save</button>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts')
<script src = "{{asset("admin/js/features/ticket-points.js")}}"></script>
@endsection

