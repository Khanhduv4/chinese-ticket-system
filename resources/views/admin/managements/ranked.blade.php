@php
	$status = ['<span class="badge badge-warning">Registered</span>','<span class="badge badge-success">Received</span>'];
@endphp
@extends('admin.layouts.master')

@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Ranked</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
				<li class="breadcrumb-item active">Ranked</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row p-3">

							<div class="col-md-8">
								<h3 class="card-title">Ranked</h3>
							</div>
							<div class="col-md-4">

							</div>

						</div>
						<div class="table-responsive p-1">
							<table id ="myTable" class="table table-bordered">
								<thead>
									<tr>
										<th class = "text-center">#</th>
										<th>Organization</th>
										<th class = "text-center">Score</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($ranked as $user)
									<tr>
										<td class = "text-center">{{$loop->index + 1}}</td>
										<td>
											{{$user->organization}}
										</td>
										<td class = "text-center">
											{{$user->total_score}}
										</td>
									</tr>
									@endforeach

								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
    </div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

@endsection

@section('scripts')
<script src = "{{asset("admin/js/features/customers.js")}}"></script>
@endsection

