@extends('admin.layouts.master')

@section('styles')
<link href="{{asset('admin/plugins/summernote/dist/summernote.css')}}" rel="stylesheet" />

@endsection
@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Create resources</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
				<li class="breadcrumb-item active">Create resources</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row p-3">
							<div class="col-md-8">
								<h3 class="card-title">resource information</h3>
							</div>
							<div class="col-md-4">
								<button type="submit" form="infoForm" data-id = {{$post->id ?? "0"}} id = "saveBtn" data-function="{{$method}}" class="btn waves-effect waves-light btn-primary pull-right">Save</button>
							</div>
						</div>
						<form class="floating-labels m-t-40 px-3" id = "infoForm" novalidate>
							<div class="row">
								{{-- <div class="form-group col-9 m-b-40">
									<input type="text" class="form-control" required name = "title" value="@if(isset($post)){{ $post->title}}@endif" />
										<span class="bar"></span>
										<label for="input1">Title</label>
									<p class="help-block"></p>
									</div> --}}
									<div class="form-group col-md-4 m-b-40">
										<input type="number" class="form-control" required name = "news_feed" value="{{$post->news_feed ?? 0}}" />
											<span class="bar"></span>
											<label for="input1">New resources</label>
										<p class="help-block"></p>

									</div>
									<div class="form-group col-md-4 m-b-40">
										<input type="number" class="form-control" required name = "media" value="{{$post->media ?? 0}}" />
											<span class="bar"></span>
											<label for="input1">Media (3 points)</label>
										<p class="help-block"></p>

									</div>
									<div class="form-group col-md-4 m-b-40">
										<input type="number" class="form-control" required name = "website" value="{{$post->website ?? 0}}" />
											<span class="bar"></span>
											<label for="input1">Website (5 points)</label>
										<p class="help-block"></p>

									</div>

							</div>

						</form>
						<div class="summernote">
							@if(isset($post))
							{!!$post->body!!}
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

@endsection

@section('scripts')
<script src = "{{asset("admin/js/features/posts.js")}}"></script>
<script src="{{asset("admin/plugins/summernote/dist/summernote.min.js")}}"></script>
<script>
	jQuery(document).ready(function() {

		$('.summernote').summernote({
			height: 350, // set editor height
			minHeight: null, // set minimum height of editor
			maxHeight: null, // set maximum height of editor
			focus: false // set focus to editable area after initializing summernote
		});

		$('.inline-editor').summernote({
			airMode: true
		});

	});

	window.edit = function() {
			$(".click2edit").summernote()
		},
		window.save = function() {
			$(".click2edit").summernote('destroy');
		}
</script>
@endsection

