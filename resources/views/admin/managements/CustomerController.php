<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use App\Http\Controllers\Controller;
use App\Customer;
use App\Ticket;
use App\User;
use App\City;
use App\Constant;


class CustomerController extends Controller
{

    private $model;
    private $userModel;
    private $ticketModel;
    private $cityModel;

    public function __construct()
    {
        $this->model = new Customer;
        $this->userModel = new User;
        $this->cityModel = new City;
        $this->ticketModel = new Ticket;
    }

    public function view()
    {
        if (Auth::user()->role == 0)
            $data['customers'] = $this->model->get();

        else
            $data["customers"] = $this->model->getByUserId(Auth::user()->id);


        return view("admin.managements.customers", $data);
    }

    public function viewClient(Request $req)
    {
        $ref_code = $req->input('ref');
        if (!$ref_code) {
            $ref_code = "";
        } else {
            $data['ref_code'] = $ref_code;
        }

        $data['users'] = $this->userModel->getAffiliater();
        $data['cities'] = $this->cityModel->get();

        return view("guest.register", $data);
    }

    public function viewCenter(Request $req)
    {
        $ref_code = $req->input('ref');
        if (!$ref_code) {
            $ref_code = "";
        } else {
            $data['ref_code'] = $ref_code;
        }

        $data['users'] = $this->userModel->getAffiliater();
        $data['cities'] = $this->cityModel->get();

        return view("guest.center", $data);
    }

    public function viewSignIn(Request $req)
    {
        if ($req->session()->has('phone')) {
            return redirect("/sharing");
        }

        return view("guest.signin");
    }

    public function viewSharing(Request $req)
    {

        if (!$req->session()->has('phone')) {
            return view("guest.signin");
        }

        $customer = $this->model->getByPhone($req->session()->get('phone'));
        $data["customer"] = $customer;
        $data["refs"] = $this->model->getRefs($customer->id);
        $data["top_100"] = $this->model->getTop100();
        return view("guest.sharing", $data);

    }

    public function signIn(Request $req)
    {

        if (!$req->input('phone')) return response("phone is empty !", 400);
        $customer = $this->model->getByPhone($req->input('phone'));
        if (!$customer) return response("Unregistered", 400);
        $data["customer"] = $customer;

        $req->session()->put('phone', $customer->phone);

        return response(["status" => "OK"], 200);

    }

    public function getById(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $point = $this->model->get($req->input('id'));
        if ($point)
            return response(["status" => "OK", "point" => $point], 200);
    }

    public function search(Request $req)
    {
        if ($req->input('phone') == NULL) return response("Phone is invalid !", 400);
        $customer = $this->model->getByPhone($req->input('phone'));
        if ($customer)
            return response(["status" => "OK", "customer" => $customer], 200);
        return response(["status" => "OK"], 404);
    }

    public function getTicketPoints()
    {
        if (!Auth::check())
            return 0;
        else {
            if (Auth::user()->role == 0) {
                return $this->ticketPointModel->get();
            } else {
                return $this->ticketPointModel->getByUserId(Auth::user()->id);
            }
        }
    }

    public function create(Request $req)
    {
        if (!isset($req)) return response("Request is invalid !", 400);

        $customer = new Customer();

        $ref_guest = $this->model->getByRefCode($req->input('refCode'));

        if (!$ref_guest) {
            $customer->ref_id = null;
        } else {
            $customer->ref_id = $ref_guest->id;
        }
        $customer->name = $req->input("name");
        $customer->phone = $req->input("phone");
        $customer->organization = $req->input("organization");
        $customer->title = $req->input("title");
        $customer->ref_code = sha1(time());

        $customer->save();
        $req->session()->put('phone', $customer->phone);
        return response(['guest' => $customer], 200);
    }

    public function giveTicket(Request $req)
    {
        if ($req->input('id') == NULL) return response("Id is null !", 400);
        $customer = Customer::find($req->input('id'));
        if ($customer->ref_id) {
            $ref = Customer::find($customer->ref_id);
            $ref->score += 1;
            $ref->save();
            $this->checkVIP($customer->ref_id);
        }
        $ticket = Ticket::find($req->input('ticket_id'));

        if ($ticket) {
            $ticket->guest_id = $customer->id;
            $ticket->save();
        } else {
            return response(["status" => 1], 200);
        }
        $customer->ticket_status = Constant::TICKET_STATUS_COLLECTED;
        $customer->user_id = Auth::user()->id;
        $user = Auth::user();
        $user->score += 10;

        $user->save();
        $customer->save();

        return response(["status" => 0], 200);
    }

    public function checkVIP($guestId)
    {
        if ($this->model->countRefs($guestId) >= 10) {
            $guest = $this->model->get($guestId);
            if ($guest->ticket_type == Constant::TICKET_TYPE_NORMAL) {
                $guest->ticket_type = Constant::TICKET_TYPE_VIP;
                $guest->save();
            }
            return true;
        } else {
            return false;
        }
    }

    public function delete(Request $req)
    {
        if ($req->input('id') == NULL) return response("ID is invalid !", 400);
        $customer = Customer::find($req->input('id'));
        if ($customer)
            $customer->delete();
        return response("OK", 200);
    }

}

