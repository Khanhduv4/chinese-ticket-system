@php
	$status = ['<button type="button" class="btn btn-xs waves-effect waves-light btn-rounded btn-warning">Pending</button>', '<button type="button" class="btn btn-xs waves-effect waves-light btn-rounded btn-primary">Reviewed</button>']
@endphp
@extends('admin.layouts.master')

@section('styles')
<style>
	.post-information img {
		max-width:100%!important;
	}

</style>
@endsection

@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Activities Management</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
				<li class="breadcrumb-item active">Posts</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row p-3">
							<div class="col-md-8">
								<h3 class="card-title">Activities List</h3>
							</div>
							<div class="col-md-4">
								<a href="/admin/posts/create">
									<button type="button" class="btn waves-effect waves-light btn-primary pull-right">Create resources</button>
								</a>
							</div>
							<!-- sample modal content -->
							<div class="modal fade" id ="infoModal" tabindex="-1" role="dialog" aria-labelledby="postInfoModalLabel" aria-hidden="true" style="display: none;">
								<div class="modal-dialog modal-lg">
									<div class="preloader">
										<svg class="circular" viewbox="25 25 50 50">
											<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
									</div>
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="myLargeModalLabel">Review activity</h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										</div>
										<div class="modal-body">
											<div class="card card-outline-info">
												<div class="card-header">
													<h4 class="m-b-0 text-white">Activity Infomation</h4></div>
												<div class="card-body post-information">
													<div class="row">
														<div class="col-6 b-r"> <strong>Title</strong>
															<br>
															<p class="text-muted post-title" name = "title"></p>
														</div>
														<div class="col-6"> <strong>Score</strong>
															<br>
															<p class="text-muted post-score" name = "score"></p>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-12 b-r">
															<div class = "mb-3">
																<strong>Activity content</strong>
															</div>
															<div class="post-body">
															</div>
														</div>

													</div>
												</div>
											</div>
											<div class="card card-outline-info">
												<div class="card-header">
													<h4 class="m-b-0 text-white">Review</h4></div>
												<div class="card-body poster-information">
													<form id = "reviewForm" class="form-horizontal row" novalidate>
														<input id = "reviewed-post-id" type="hidden" value="">
														<div class="form-group col-md-12">
															<label>Message</label>
															<textarea name = "message" class="form-control" rows="5"></textarea>
														</div>
													</form>
											
												</div>
											</div>
										</div>

										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button data-status = 2 type="button" class="saveReviewBtn btn btn-danger">Disapprove</button>

											<button data-status = 1 type="button" class="saveReviewBtn btn btn-success">Approve</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
						</div>
						<div class="table-responsive p-1">
							<table id ="myTable" class="table table-bordered w-auto">
								<thead>
									<tr>
										<th class = "text-center">#</th>
										<th>Title</th>
										@if (Auth::user()->role == 0) 
										<th class = "text-center">Posted By</th>
										@endif
										<th class = "text-center">Status</th>
										<th class = "text-center">Score</th>
										<th class = "text-center">Action</th>
										<th class = "text-right">Created At</th>
										<th class = "text-right">Update At</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($posts as $post)
									<tr>
										<td class = "text-center">{{$loop->index + 1}}</td>
										<td>
											Newsfeed: {{$post->news_feed}} Media: {{$post->media}} Website: {{$post->website}}
										</td>
										@if (Auth::user()->role == 0) 
										<td data-toggle="tooltip" title="Address: {{$post->user->address}}" class = "text-center">
											{{$post->user->name}}
										</td>
										@endif
										<td class = "text-center">
											@switch($post->status)
											@case(Constants::POST_STATUS_PENDING)
											<button type="button" class="btn btn-xs waves-effect waves-light btn-rounded btn-warning">Pending</button>
											@break
											@case(Constants::POST_STATUS_APPROVED)
											<button class="getMessageBtn btn btn-xs waves-effect waves-light btn-rounded btn-primary">Approved</button>
											@break
											@case(Constants::POST_STATUS_DISAPPROVED)
											<button type="button" class="btn btn-xs waves-effect waves-light btn-rounded btn-danger">Disapproved</button>
											@break
											@default
												Khanhs
											@endswitch
										</td>
										<td class = "text-center">
											{{$post->score}}
										</td>
										<td class = "text-center product-action">
											@if(Auth::user()->role == 0)
											<i data-id = {{$post->id}} data-toggle="modal" data-target="#infoModal" class="reviewBtn fa fa-check text-info m-r-10"></i>
											@endif
											@if($post->status != Constants::POST_STATUS_PENDING)
											<i data-message = "{{$post->message}}" data-status = "{{$post->status}}" class="getMessageBtn fa fa-comment text-success m-r-10"></i>
											@endif
											@if($post->status != Constants::POST_STATUS_APPROVED && Auth::user()->role != 0)
											<a href="/admin/posts/edit/{{$post->id}}"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
											<a href="" data-id = {{$post->id}} class = "deleteBtn"> <i class="fa fa-close text-danger"></i> </a>
											@endif
										</td>
                                        <td class = "text-right">
                                            {{date('H:i:s d/m/Y', strtotime($post->created_at))}}
                                        </td>
                                        <td class = "text-right">
                                            {{date('H:i:s d/m/Y', strtotime($post->updated_at))}}
                                        </td>
						
									</tr>
									@endforeach

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

@endsection

@section('scripts')

<script src = "{{asset("admin/js/features/posts.js")}}"></script>
@endsection

