@php
	$status = ['<span class="badge badge-warning">Registered</span>','<span class="badge badge-success">Received</span>'];
@endphp
@extends('admin.layouts.master')

@section('content')

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Guest Management</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
				<li class="breadcrumb-item active">Guest</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
					@if (Auth::user()->role != 0)
						<div class="row">
							<div class="col-12">
								<form id = "searchForm">
									<div class="input-group">
										<div class="input-group-prepend">
											<button class="btn btn-danger" type="button">Guest Info</button>
										</div>
										<input type="text" class="form-control" required="" id = "searchText" placeholder="Enter guest's phone number">
										<div class="input-group-append">
											<button class="btn btn-success" id ="searchBtn" type="submit">Search</button>
										</div>
										<div class="help-block"></div>
									</div>
								</form>

							</div>
						</div>
						<div id = "infoPanel" class="row pt-5 hidden-xs-up">
							<div class="col-12">
								<div class="ribbon-wrapper card">
									<div  class="ribbon ribbon-info">Customer Info</div>
									<div class="card-body">
										<div class="row">
											<div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
												<br>
												<p class="text-muted" name = "name"></p>
											</div>
											<div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
												<br>
												<p class="text-muted" name = "phone"></p>
											</div>
											<div class="col-md-3 col-xs-6 b-r"> <strong>Organization</strong>
												<br>
												<p class="text-muted" name = "organization"></p>
											</div>
											<div class="col-md-3 col-xs-6"> <strong>Title</strong>
												<br>
												<p class="text-muted" name = "title"></p>
											</div>
										</div>
										<hr>
										<div class="row">
											<div class="col-md-6 b-r">
												<div class="pull-left">
													<p>Ticket status</p>
												</div>
												<div class="pull-right" name = "ticket-status">

												</div>
											</div>
											<div class="col-md-6">
												<div class="pull-left">
													<p>Ticket type</p>
												</div>
												<div class="pull-right" name = "ticket-type">
												</div>
											</div>
										</div>

									</div>
								</div>

							</div>
						</div>
					@else
						<div class="row p-3">

							<div class="col-md-8">
								<h3 class="card-title">Customers List</h3>
							</div>
							<div class="col-md-4">
								<button type="button" class="btn waves-effect waves-light btn-primary pull-right" data-toggle="modal" data-target="#infoModal">Create Customer</button>

							</div>

						</div>
						<div class="table-responsive p-1">
							<table id ="myTable" class="table table-bordered">
								<thead>
									<tr>
										<th class = "text-center">#</th>
										<th>Name</th>
										<th class = "text-right">Phone</th>
										<th class = "text-right">Status</th>
										<th class = "text-right">Created At</th>
										<th class = "text-right">Update At</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($customers as $customer)
									<tr>
										<td class = "text-center">{{$loop->index + 1}}</td>
										<td>
											{{$customer->name}}
										</td>
										<td class = "text-right">
											{{$customer->phone}}
										</td>
										<td class = "text-right">
											{!!$status[$customer->ticket_status]!!}
										</td>
										<td class = "text-right">
											{{date('H:i:s d/m/Y', strtotime($customer->created_at))}}
										</td>
										<td class = "text-right">
											{{date('H:i:s d/m/Y', strtotime($customer->updated_at))}}
										</td>


									</tr>
									@endforeach

								</tbody>
							</table>
						</div>

					@endif
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
    </div>

    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
        <div class="modal-dialog" role="document">
            <div class="preloader">
                <svg class="circular" viewbox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Customer Info</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form class = "form-material p-1" id = "infoForm">
                        <div class="form-group">
                            <label for="name" class="control-label">Name:</label>
                            <input type="text" placeholder="Example: Tran A" class="form-control" name = "name" />
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label">Phone Number:</label>
                            <input type="phone" placeholder="Example: 87128378122" class="form-control" name = "phone" />
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">Organization:</label>
                            <input type="email" placeholder="Example: trana@domain.com" class="form-control" name = "organization" />
                        </div>
                        <div class="form-group">
                            <label for="ticket-point" class="control-label">Title:</label>
                            <select name = "title" class="form-control">
                                {{-- @foreach($ticketPoints as $ticketPoint)
                                    <option value="{{$ticketPoint->id}}">{{$ticketPoint->address}}</option>

                                @endforeach --}}
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" data-function = "create" id = "saveBtn" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ticketLeft" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
        <div class="modal-dialog" role="document">
            <div class="preloader">
                <svg class="circular" viewbox="25 25 50 50">
                    <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
            </div>
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Choose ticket</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <form class = "form-material p-1" id = "infoForm">

                        <div class="form-group">
                            <label>Code</label>
                            <select name = "ticket" class="form-control">
                            </select>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" data-function = "create" id = "giveTicketBtn" class="btn btn-primary">Give ticket</button>
                </div>
            </div>
        </div>
    </div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

@endsection

@section('scripts')
<script src = "{{asset("admin/js/features/customers.js")}}"></script>
@endsection

