@extends('guest.master') @section('content')

<!--hero section start-->
<section
    class="ImageBackground ImageBackground--overlay u-height700- js-Parallax fullscreen"
    data-overlay="5"
>
    <div class="ImageBackground__holder">
        <img src="{{ asset('guest/assets/imgs/banner/ban1.jpg') }}" alt="" />
    </div>
    <div
        class="container u-vCenter- u-PaddingTop100 u-sm-PaddingTop30 u-PaddingBottom100 u-sm-PaddingBottom30"
    >
        <div
            id="registerPanel"
            class="row text-left u-PaddingTop100 u-sm-PaddingTop30 u-PaddingBottom100 u-sm-PaddingBottom30"
        >
            <div class="col-md-7 text-white">
                <h2
                    class="u-FontSize75 u-xs-FontSize40 u-Weight700 u-MarginTop100 u-xs-MarginTop30 u-MarginBottom0"
                >
                    <!--Annual Dev Conference 2017-->
                </h2>
                <h3
                    class="text-uppercase text-white u-Weight700 u-MarginTop0 u-MarginBottom50"
                >
                    <!--15 - 19 July, 2017, San
					Francisco, CA -->
                </h3>
            </div>

            <div class="col-md-5">
                <div
                    class="bg-black-transparent u-BorderRadius4 u-BoxShadow40 u-Padding10"
                >
                    <div
                        class="row u-PaddingTop30 u-PaddingLeft30 u-PaddingRight30"
                    >
                        <div class="col-md-12">
                            <form id="registerForm">
                                {{ csrf_field() }}
                                <input type="hidden" id="refCode" value="{{$ref_code ?? ""}}">

                                <h3 class="text-white u-MarginTop0">注册</h3>
                                <div class="form-group">
                                    <input
                                        class="form-control form-control--white"
                                        name="name"
                                        placeholder="姓名"
                                        type="text"
                                    />
                                </div>
                                <div class="form-group">
                                    <input
                                        class="form-control form-control--white"
                                        name="phone"
                                        placeholder="电话"
                                        type="text"
                                    />
                                </div>

                                <div class="form-group">
                                    <input
                                        class="form-control form-control--white"
                                        name="organization"
                                        placeholder="单位"
                                        type="text"
                                        type="text"
                                    />
                                </div>

                                <div class="form-group">
                                    <div class="row mb-2">
                                        <div class="col-xs-7">
                                            <input
                                                type="text"
                                                class="form-control"
                                                placeholder="验证码"
                                                name="otp"
                                            />
                                        </div>
                                        <div class="col-xs-5">
                                            <button
                                                class="btn btn-default btn-block"
                                                style = "padding: 0 10px"
						type="button"
                                                id="getOTPBtn"
                                            >获取验证码
                                            </button>
                                        </div>
                                    </div>
                                    <small class = "text-white hidden">注：如您收不到验证码，请联系组委会小敦：13007609501</small>
                                </div>

                                <div class="form-group">
                                    <button
                                        type="button"
                                        id="registerBtn"
                                        class="btn btn-primary btn-block"
                                    >
                                        注册
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- <div class="col-12 hidden" id="infoPanel">
            <div
                class="bg-black-transparent u-BorderRadius4 u-BoxShadow40 u-Padding10"
            >
                <div
                    class="row u-PaddingTop30 u-PaddingLeft30 u-PaddingRight30"
                >
                    <div class="col-md-12">
                        <form>
                            <h3 class="text-white u-MarginTop0">分享信息</h3>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="speaker-image">
                                            <img
                                                class="qrCode"
                                                src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=https://pw.zzfexpo.gk18.cn"
                                                alt=""
                                            />
                                        </div>
                                    </div>
                                    <div class="col-md-9 text-white">
                                        <h3
                                            class="u-MarginTop0 u-xs-MarginTop20"
                                        >
                                            恭喜您！注册成功！
                                        </h3>
                                        <p>
                                            <em
                                                >分享此链接给您的朋友，获取VIP门票</em
                                            >
                                        </p>
                                        <div class="row mt-4">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input
                                                        class="form-control sharingLink"
                                                        placeholder="你的分享链接"
                                                    />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <button
                                                        type="button"
                                                        class="btn btn-primary btn-block copyBtn"
                                                    >
                                                        复制
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <p style="margin-top: 0">
                                            <em
                                                >分享此链接给您的朋友，获取VIP门票</em
                                            >
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
</section>
<!--hero section end-->

@endsection
