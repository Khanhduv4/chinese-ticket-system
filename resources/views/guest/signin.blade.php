@extends('guest.master')

@section('content')


<!--hero section start-->
<section class=" ImageBackground ImageBackground--overlay u-height700- js-Parallax fullscreen" data-overlay="5">
	<div class="ImageBackground__holder">
		<img src="{{asset('guest/assets/imgs/banner/ban1.jpg')}}" alt="" />
	</div>
	<div class="container u-vCenter- u-PaddingTop100 u-sm-PaddingTop30 u-PaddingBottom100 u-sm-PaddingBottom30">

        <div class="col-md-8 col-md-offset-2" id="signInPanel">
            <div class="bg-black-transparent u-BorderRadius4 u-BoxShadow40 u-Padding10">
                <div class="row u-PaddingTop30 u-PaddingLeft30 u-PaddingRight30">
                    <div class="col-md-12">
					
                        <form>
				{{ csrf_field() }}
                            <h3 class="text-white u-MarginTop0">登录</h3>
                                <div class="row mt-4">
                                        <div class="form-group col-md-9">
                                            <input class="form-control" name = "phone" placeholder="电话">
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <button type = "button" id ="signInBtn" class="btn btn-primary btn-block">登录</button>
                                            </div>
                                        </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
	</div>
</section>
<!--hero section end-->

@endsection
