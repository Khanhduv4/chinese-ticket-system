@extends('guest.master')

@section("styles")
<style>
    nav.navbar.bootsnav.no-background.white .attr-nav>ul>li>a,
    nav.navbar.bootsnav.navbar-transparent.white .attr-nav>ul>li>a,
    nav.navbar.bootsnav.navbar-transparent.white ul.nav>li>a,
    nav.navbar.bootsnav.no-background.white ul.nav>li>a {
        color: grey;
    }

    .nav-pills>li.active>a,
    .nav-pills>li.active>a:focus,
    .nav-pills>li.active>a:hover {
        background-color: #f90048;
    }

    .tab-content {
        padding: 0;
        border: 0;
        background-color: #fff;
    }
</style>
@endsection


@section('content')

<!--page title start-->
<section class="ImageBackground ImageBackground--overlay v-align-parent u-height350 u-BoxShadow40" data-overlay="0">
    <div class="ImageBackground__holder"
        style="background-image: url({{asset('guest/assets/imgs/banner/inner-banner.jpg')}});">
        <img src="{{asset('guest/assets/imgs/banner/inner-banner.jpg')}}" alt="">
    </div>
    <div class="v-align-child">
        <div class="container ">
            <div class="row ">
                <div class="col-md-12 text-center">
                    <h1 class="text-uppercase u-Margin0 u-Weight700">个人中心</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="wrapper">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-8 col-md-offset-2">
                <h1 class=" u-MarginBottom10 u-Weight700">恭喜您注册成功 {{$customer->name}} !</h1>
                <div class="Split Split--height2"></div>
            </div>
            <div
                class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 u-MarginBottom50 u-xs-MarginBottom30 u-sm-MarginBottom30">
                <p class="u-PaddingTop30">您的分享链接</p>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-2 u-MarginBottom35">
                <div class="u-BoxShadow100">
                    <div class="Blurb u-InlineBlock">
                        <a class="open-popup-link hover-img">
                            <figure>
                                <img class="img-responsive qrCode mb-3"
                                    src="https://api.qrserver.com/v1/create-qr-code/?size=350x350&data=https://pw.zzfexpo.com?ref={{$customer->ref_code}}"
                                    alt="...">
                            </figure>
                        </a>
                        <a class="create-invitation-button" href=""><span
                                class="btn btn-sm btn-primary n-MarginTop5 text-uppercase w-100">分享二维码</span></a>
                    </div>
                </div>
            </div>

            <div class="col-md-10">
                <div class="row">
                    <div class="form-group col-md-9">
                        <input class="form-control sharingLink" value = "https://pw.zzfexpo.com?ref={{$customer->ref_code}}" name = "phone" placeholder="您的分享链接">
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <button type = "button" id ="signInBtn" class="btn btn-primary btn-block copyBtn">复制</button>
                        </div>
                    </div>
                </div>
                <div class="bg-primary bg-primary--gradient px-5 u-PaddingTop40 u-PaddingBottom40">
                    <div class="row">
                        <div class="col-md-4 u-sm-MarginBottom30">
                            <div class="media u-OverflowVisible">
                                <div class="media-left">
                                    <div class="Thumb">
                                        <i style="font-size: 45px" class="fa fa-trophy"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h4 class="u-MarginTop0 u-MarginBottom5 u-Weight700"> 您的排名</h4>
                                    <small name="rank">{{$customer->rank}}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 u-sm-MarginBottom30">
                            <div class="media u-OverflowVisible">
                                <div class="media-left">
                                    <div class="Thumb">
                                        <i style="font-size: 45px" class="far fa-clipboard-check"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h4 class="u-MarginTop0 u-MarginBottom5 u-Weight700"> 已领取门票人数</h4>
                                    <small name="collected">{{$customer->collected_ticket}}</small>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 u-sm-MarginBottom30">
                            <div class="media u-OverflowVisible">
                                <div class="media-left">
                                    <div class="Thumb">
                                        <i style="font-size: 45px" class="fas fa-user-tie"></i>
                                    </div>
                                </div>
                                <div class="media-body">
                                    <h4 class="u-MarginTop0 u-MarginBottom5 u-Weight700"> 已邀请人数</h4>
                                    <small name="refs">{{$customer->refs_count}}</small>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>

        <div id="invitationPanel" class="row mt-5 hidden">
            <div class="hidden">
                <img crossorigin id="template1" src="{{asset("guest/assets/imgs/invitation/template1.jpg")}}" alt="">
                <img crossorigin id="template2" src="{{asset("guest/assets/imgs/invitation/template2.jpg")}}" alt="">
            </div>
            <div class="col-md-6">
                <a download="invitation.jpg" href="">
                    <img id="invitation1" style="width:100%" src="" alt="">
                </a>

            </div>
            <div class="col-md-6">
                <a download="invitation.jpg" href="">
                    <img id="invitation2" style="width:100%" src="" alt="">
                </a>
            </div>
        </div>
        <div class="py-5">
            <ul id="myTabs" class="nav nav-pills nav-justified mb-3" role="tablist" data-tabs="tabs">

                <li class="active"><a href="#pills-100" data-toggle="tab">邀请列表</a></li>
                <li><a href="#pills-refs" data-toggle="tab">邀请记录</a></li>

            </ul>
            <div class="tab-content">

                <div role="tabpanel" class="tab-pane fade in active" id="pills-100">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered u-Margin0">
                            <thead>
                                <tr>
                                    <th class="text-center">序号</th>
                                    <th>名称</th>
                                    <th class="text-center">受邀请人</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($top_100 as $item)
                                <tr>
                                    <td class="text-center">
                                        {{$loop->index + 1}}
                                    </td>
                                    <td>
                                        {{$item->name}}
                                    </td>
                                    <td class="text-center">
                                        {{$item->refs}}
                                    </td>
                                </tr>


                                @endforeach

                            </tbody>
                        </table>
                    </div>

                </div>

                <div role="tabpanel" class="tab-pane fade" id="pills-refs">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered u-Margin0">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>名称</th>
                                    <th class="text-center">邀请时间</th>
                                    <th class="text-center">状态</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($refs as $item)

                                <tr>
                                    <td class="text-center">
                                        {{$loop->index + 1}}
                                    </td>
                                    <td>
                                        {{$item->name}}
                                    </td>
                                    <td class="text-center">
                                        {{date('d/m/Y', strtotime($item->created_at))}}
                                    </td>
                                    <td>
                                        {{$item->status}}
                                    </td>
                                </tr>


                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <!-- 1st day start -->


                </div>

            </div>
        </div>
    </div>

</div>
<!--page title end-->



@endsection
