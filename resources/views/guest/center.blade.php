@extends('guest.master')

@section("styles")
<style>
    nav.navbar.bootsnav.no-background.white .attr-nav>ul>li>a,
    nav.navbar.bootsnav.navbar-transparent.white .attr-nav>ul>li>a,
    nav.navbar.bootsnav.navbar-transparent.white ul.nav>li>a,
    nav.navbar.bootsnav.no-background.white ul.nav>li>a {
        color: grey;
    }
    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
        background-color: #f90048;
    }
</style>
@endsection

@section('content')

<!--page title start-->
<section class="ImageBackground ImageBackground--overlay v-align-parent u-height350 u-BoxShadow40" data-overlay="0">
    <div class="ImageBackground__holder"
        style="background-image: url({{asset('guest/assets/imgs/banner/inner-banner.jpg')}});">
        <img src="{{asset('guest/assets/imgs/banner/inner-banner.jpg')}}" alt="">
    </div>
    <div class="v-align-child">
        <div class="container ">
            <div class="row ">
                <div class="col-md-12 text-center">
                    <h1 class="text-uppercase u-Margin0 u-Weight700">领票点查询</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<!--page title end-->


<div class="container py-5">
    <ul id="myTabs" class="nav nav-pills mb-3" role="tablist" data-tabs="tabs">
        @foreach ($cities as $city)
        @if ($loop->first)
        <li class="active"><a href="#pills-{{$city->id}}" data-toggle="tab">{{$city->name}}</a></li>

        @else
        <li><a href="#pills-{{$city->id}}" data-toggle="tab">{{$city->name}}</a></li>

        @endif
        @endforeach
    </ul>
    <div class="tab-content">
        @foreach ($cities as $city)
        <div role="tabpanel" class="tab-pane fade @if ($loop->first) in active @endif" id="pills-{{$city->id}}">
            @foreach ($users as $user)
            <!-- 1st day start -->
            @if ($user->city_id == $city->id)
            <div class="row mb-3">
                <div class="col-md-3 pl-md-5 pt-2">
                    <div class="mb-2">
                        <p><i class="fa fa-user-circle mr-2"></i>
                            {{$user->name}}</p>
                    </div>
                    <div class="">
                        <p><i class="fa fa-phone-square mr-2"></i>
                            {{$user->phone}}</p>
                    </div>
                </div>
                <div class="col-md-7 mt-4 mt-md-0">
                    <h4 class="my-1">{{$user->code}} | {{$user->organization}}</h4>
                    <p><em>{{$city->name}}</em></p>
                    <h6 class="mt-1 mb-1">{{$user->address}}</h6>
                </div>
                <div class="col-md-2 pt-2">
                    <h6>剩余门票: <span class="ticket-left">{{$user->ticket_left}}</span></h6>
                </div>
            </div>

            <hr>
            @endif
            @endforeach
        </div>
        @endforeach
    </div>
</div>



@endsection
