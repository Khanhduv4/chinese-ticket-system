<!DOCTYPE html>
<html class="html">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="assets/imgs/favicon.png" />

    <title>郑州理财博览会</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('guest/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('guest/assets/vendor/bootsnav/css/bootsnav.css')}}">

    <link rel="stylesheet" href="{{asset('guest/assets/vendor/custom-icon/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('guest/assets/vendor/owl.carousel2/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('guest/assets/vendor/magnific-popup/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('guest/assets/vendor/animate.css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('guest/assets/vendor/swiper/css/swiper.min.css')}}">
    <link href="{{asset("admin/plugins/sweetalert/sweetalert.css")}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('guest/assets/css/main.min.css')}}">
    <link rel="stylesheet" href="{{asset('guest/assets/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('guest/assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{asset('guest/assets/css/grid.css')}}">

    <!-- endinject -->

    @yield("styles")

</head>

<body>

    <!--header start-->
    <div class="sticky-wrapper" style="height: 0px;">
        <header id="header" class="Sticky hidden-xs">
            <!-- Start Navigation -->
            <nav class="navbar navbar-default navbar-fixed white no-background bootsnav">

                <div class="container">
                    <!-- Start Atribute Navigation -->
                    <div class="attr-nav">
                        <ul>
                            <li class="buy-btn hidden-xs hidden-sm"><a href="/"><span class="btn btn-sm btn-primary n-MarginTop5 text-uppercase">注册</span></a></li>
                        </ul>
                    </div>
                    <!-- End Atribute Navigation -->

                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="/">
                            <img src="{{asset('guest/assets/imgs/logo.png')}}" class="logo logo-display" alt="">
                            <img src="{{asset('guest/assets/imgs/logo.png')}}" class="logo logo-scrolled" alt="">
                        </a>
                    </div>
                    <!-- End Header Navigation -->

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-right" data-in="" data-out="">
                            <li>
                                <a href="/signin" class="dropdown-toggle" data-toggle="dropdown">登录</a>
                            </li>
                            <li>
                                <a href="/center" class="dropdown-toggle" data-toggle="dropdown">领票点</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
            </nav>
            <!-- End Navigation -->
            <div class="clearfix"></div>
        </header></div>

    <!--header end-->

    <div class="wrapper">

        @yield('content')
    </div>

    <nav class="mobile-bottom-nav hidden-md-up">
        <div class="mobile-bottom-nav__item mobile-bottom-nav__item--active">
            <a href="/">
                <div class="mobile-bottom-nav__item-content">
                    <i style = "font-size: 25px" class="fa fa-comment-edit"></i>
                    注册
                </div>
            </a>
        </div>
        <div class="mobile-bottom-nav__item">
            <a href="/center">
                <div class="mobile-bottom-nav__item-content">
                    <i style = "font-size: 25px" class="fa fa-map-marked-alt"></i>
                    领票点
                </div>
            </a>

        </div>
        <div class="mobile-bottom-nav__item">
            <a href="/sharing">
                <div class="mobile-bottom-nav__item-content">
                    <i style = "font-size: 25px" class="fas fa-share-alt-square"></i>
                    登录
                </div>
            </a>
        </div>
    </nav>


    <!-- inject:js -->
    <script src="{{asset('guest/assets/vendor/jquery/jquery-1.12.0.min.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/bootsnav/js/bootsnav.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/waypoints/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/waypoints/sticky.min.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/headroom/headroom.min.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/jquery.countTo/jquery.countTo.min.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/owl.carousel2/owl.carousel.min.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/jquery.appear/jquery.appear.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/isotope/isotope.pkgd.min.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/imagesloaded/imagesloaded.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('guest/assets/vendor/swiper/js/swiper.min.js')}}"></script>
    <script src="{{asset("admin/plugins/sweetalert/sweetalert.min.js")}}"></script>
    <script src="{{asset('guest/assets/js/main.js')}}"></script>
    <script src="{{asset('guest/assets/js/guest.js')}}?disable-cache=1"></script>
    <!-- endinject -->

</body>

</html>
