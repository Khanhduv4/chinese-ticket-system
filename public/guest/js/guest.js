$(function () {
	"use strict";

	$(".loader__figure").fadeOut(); // will first fade out the loading animation
	$(".loader").delay(500).fadeOut("slow"); // will fade out the white DIV that covers the website.

	const ticketStatus = ["Registered", "Collected"];

	$("#registerBtn").on("click", function (e) {
		const button = $(this);
		const form = $("#ajax-form");
		const infoPanel = $("#infoPanel");
		// const preloader = $(".preloader");
		// preloader.css("display", "block");
		button.attr("disabled", true);
		let formData = form.serializeArray();
		let data = {};
		$(formData).each(function (index, obj) {
			data[obj.name] = obj.value;
		});
		data.refCode = form.find("#refCode").val();
		$.ajax({
			url: `/register`,
			method: "POST",
			data,
			success: function (data) {
				infoPanel.find("[name=name]").html(data.guest.name);
				infoPanel.find("[name=email]").html(data.guest.email);
				infoPanel.find("[name=phone]").html(data.guest.phone);
				infoPanel.find("[name=address]").html(data.address);
				const sharingLink = `https://${window.location.hostname}/?ref=${data.guest.ref_code}`;
				infoPanel
					.find(".qrCode")
					.attr(
						"src",
						`https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${sharingLink}`
					);
				infoPanel.find(".sharingLink").val(sharingLink);

				form.parent().parent().hide();
				infoPanel.removeClass("hidden");
			},
			error: function (data) {
				console.log(data);
				swal(
					{
						title: "失败！",
						text: `请检查您的信息，然后再试一次！`,
						type: "error",
					},
					function () {
						button.attr("disabled", false);
					}
				);
			},
			complete: function (data) {
				// preloader.css("display", "none");
			},
		});
	});

	$("#signInBtn").on("click", function () {
		const button = $(this);
		button.attr("disabled", true);
		const phone = $("#signInPhone").val();
		if (!phone) return;
		$.ajax({
			url: "/guest/signIn",
			method: "GET",
			data: {
				phone,
			},
			dataType: "json",
			success: function (data) {
				const sharingInfoPanel = $("#sharingInfoPanel");
				const sharingLink = `https://${window.location.hostname}/?ref=${data.customer.ref_code}`;
				let refsTable = "";
				for (let i = 0; i < data.refs.length; i++) {
					const date = new Date(data.refs[i].created_at);
					const dateString =
						date.getFullYear() +
						"-" +
						(date.getMonth() + 1) +
						"-" +
						date.getDate() +
						" " +
						date.getHours() +
						":" +
						date.getMinutes() +
						":" +
						date.getSeconds();
					refsTable += `
					<tr>
						<td>${i + 1}</td>
						<td>${data.refs[i].name}</td>
						<td>${dateString}</td>
						<td>${ticketStatus[data.refs[i].ticket_status]}</td>
					</tr>
					`;
				}
				refsTable = `
				<table class="table table-hover table-bordered">
					<thead>
						<tr>
							<th>序号</th>
							<th>姓名</th>
							<th>注册于</th>
							<th>是否领取门票</th>
						</tr>
					</thead>
					<tbody>
						${refsTable}
					</tbody>
				</table>`;
				$(".refsTable").append(refsTable);

				let top100Table = "";

				for (let i = 0; i < data.top_100.length; i++) {
					if ((data.top_100[i].id = null)) continue;
					top100Table += `
					<tr>
						<td>${i + 1}</td>
						<td>${data.top_100[i].name}</td>
						<td>${data.top_100[i].refs}</td>
					</tr>
					`;
				}

				top100Table = `
				<table class="table table-hover table-bordered">
					<thead>
						<tr>
							<th>排名</th>
							<th>姓名</th>
							<th>已邀请嘉宾人数</th>
						</tr>
					</thead>
					<tbody>
						${top100Table}
					</tbody>
				</table>`;

				$(".top100Table").append(top100Table);

				$("#signInPanel").hide();
				sharingInfoPanel.removeClass("hidden");
				sharingInfoPanel.find("[name=sharingLink]").val(sharingLink);
				sharingInfoPanel
					.find(".qrCode")
					.attr(
						"src",
						`https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${sharingLink}`
					);
				sharingInfoPanel.find("[name=name]").text(data.customer.name);
				sharingInfoPanel
					.find("[name=refs]")
					.text(data.customer.refs_count);
				sharingInfoPanel
					.find("[name=collected]")
					.text(data.customer.collected_ticket);
				sharingInfoPanel
					.find("[name=rank]")
					.text(
						(data.customer.rank.length > 0 &&
							data.customer.rank[0].stt) ||
						(!data.customer.rank.length && 0)
					);
			},
			error: function () {
				swal(
					{
						title: "失败！",
						text: `这个电话号码没有注册！`,
						type: "error",
					},
					function () {
						button.attr("disabled", false);
					}
				);
			},
		});
	});

	$(".copyBtn").on("click", function () {
		/* Select the text field */
		const copyText = $(this)
			.parent()
			.parent()
			.parent()
			.find(".sharingLink");
		copyText.select();
		/* Copy the text inside the text field */
		document.execCommand("copy");
		swal({
			title: "Completed !",
			text: `Your link was copied to your clipboard`,
			type: "success",
		});
	});

	$(".sharingLink").on("click", function () {
		$(this).select();
	});

	function drawCanvas(qrSrc, backgroundSrc, qrX, qrY, qrWidth, qrHeight) {
		return new Promise((resolve, reject) => {
			const canvas = document.createElement("canvas");
			const ctx = canvas.getContext("2d");
			const background = new Image();
			background.crossOrigin = "Anonymous";
			background.src = backgroundSrc;
			const qr = new Image();
			qr.crossOrigin = "Anonymous";
			qr.src = qrSrc;
			background.onload = () => {
				qr.onload = () => {
					canvas.width = background.width;
					canvas.height = background.height;
					ctx.drawImage(
						background,
						0,
						0,
						canvas.width,
						canvas.height
					);
					ctx.drawImage(qr, qrX, qrY, qrWidth, qrHeight);
					resolve(canvas.toDataURL("image/png"));
				};
			};
		});
	}

	$(".create-invitation-button").on("click", function () {
		const template1 = document.getElementById("template1");
		const template2 = document.getElementById("template2");
		const qrCode = document.getElementsByClassName("qrCode")[1];

		drawCanvas(qrCode.src, template1.src, 526, 1116, 120, 120).then(
			(image) => {
				document.getElementById("invitation1").src = image;
				$("#invitation1").parent().attr("href", image);
			}
		);
		drawCanvas(qrCode.src, template2.src, 518, 1134, 150, 150).then(
			(image) => {
				document.getElementById("invitation2").src = image;
				$("#invitation2").parent().attr("href", image);
			}
		);
		$("#invitationPanel").removeClass("hidden");

	});
	$("#resetBtn").on("click", function () {
		$("#registerForm")[0].reset();
	});
});

String.prototype.replaceAll = function (f, r) {
	return this.split(f).join(r);
};
