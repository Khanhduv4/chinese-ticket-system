$(function () {
    "use strict";

    $(".loader__figure").fadeOut(); // will first fade out the loading animation
    $(".loader").delay(500).fadeOut("slow"); // will fade out the white DIV that covers the website.

    const ticketStatus = ["Registered", "Collected"];

    $("#registerBtn").on("click", function (e) {
        const button = $(this);
        const form = $("#registerForm");
        const infoPanel = $("#infoPanel");
        button.attr("disabled", true);
        let formData = form.serializeArray();
        let data = {};
        $(formData).each(function (index, obj) {
            data[obj.name] = obj.value;
        });

        if (!checkPhoneNumber(data.phone)) {
            swal(
                {
                    title: "失败!",
                    text: `请输入正确的电话号码!`,
                    type: "error",
                },
                function () {
                    button.attr("disabled", false);
                }
            );
            return;
        }
        data.refCode = form.find("#refCode").val();
        $.ajax({
            url: `/register`,
            method: "POST",
            data,
            success: function (data) {
                swal(
                    {
                        title: "成功！",
                        text: `注册成功！`,
                        type: "success",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
                document.cookie = `phone=${data.guest.phone}`;
                location.href = "/sharing";
                // infoPanel.find("[name=name]").html(data.guest.name);
                // infoPanel.find("[name=email]").html(data.guest.email);
                // infoPanel.find("[name=phone]").html(data.guest.phone);
                // infoPanel.find("[name=address]").html(data.address);
                // const sharingLink = `https://${window.location.hostname}/?ref=${data.guest.ref_code}`;
                // infoPanel
                // 	.find(".qrCode")
                // 	.attr(
                // 		"src",
                // 		`https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${sharingLink}`
                // 	);
                // infoPanel.find(".sharingLink").val(sharingLink);

                // $("#registerPanel").hide();
                // infoPanel.removeClass("hidden");
            },
            error: function (data) {
                if (data.status == 422) {
                    swal({
                        title: "失败！",
                        text: `请检查信息，然后重试！`,
                        type: "error",
                    });
                    button.attr("disabled", false);
                    return;
                }
                const response = data.responseJSON;
                switch (response.status) {
                    case 0: {
                        swal({
                            title: "失败！",
                            text: `该电话号码已被注册！`,
                            type: "error",
                        });
                        break;
                    }
                    case 2: {
                        swal({
                            title: "失败！",
                            text: `错误的OTP！`,
                            type: "error",
                        });
                        break;
                    }
                    case 1: {
                        swal({
                            title: "失败！",
                            text: `发生错误。请稍后再试！`,
                            type: "error",
                        });
                        break;
                    }
                }
                button.attr("disabled", false);
            },
            complete: function (data) {
                // preloader.css("display", "none");
            },
        });
    });

    $("#getOTPBtn").on("click", function () {
        const button = $(this);
        const buttonText = button.text();
        button.attr("disabled", true);
        const form = $("#registerForm");

        const phone = form.find("[name=phone]").val();
        form.find("small").removeClass("hidden");
        const _token = form.find("[name=_token]").val();

        if (!checkPhoneNumber(phone)) {
            swal(
                {
                    title: "失败!",
                    text: `请输入正确的电话号码!`,
                    type: "error",
                },
                function () {
                    button.attr("disabled", false);
                }
            );
            return;
        }

        $.ajax({
            url: "/guest/getOTP",
            method: "POST",
            data: {
                phone,
                _token,
            },
            dataType: "json",
            success: function (data) {
                swal({
                    title: "成功！",
                    text: `OTP已发送至电话号码${phone}`,
                    type: "success",
                });
                let time = 60;
                const timer = setInterval(function () {
                    button.text(buttonText + `(${time})`);
                    time--;
                    if (time <= 0) {
                        clearInterval(timer);
                        button.text(buttonText);
                        time = 60;
                        button.attr("disabled", false);
                    }
                }, 1000);
            },
            error: function (data) {
                const response = data.responseJSON;
                switch (response.status) {
                    case 0: {
                        swal({
                            title: "失败！",
                            text: `该电话号码已被注册！`,
                            type: "error",
                        });
                        break;
                    }
                    case 2: {
                        swal({
                            title: "失败！",
                            text: `错误的OTP！`,
                            type: "error",
                        });
                        break;
                    }
                    case 1: {
                        swal({
                            title: "失败！",
                            text: `发生错误。请稍后再试！`,
                            type: "error",
                        });
                        break;
                    }
                }
            },
        });
    });

    $("#signInBtn").on("click", function () {
        const phone = $("#signInPanel").find("[name=phone]").val();
        const button = $(this);
	const _token = $("#signInPanel").find("[name=_token]").val();
        button.attr("disabled", true);

        if (phone) {
            if (!checkPhoneNumber(phone)) {
                swal(
                    {
                        title: "失败!",
                        text: `请输入正确的电话号码!`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
                return;
            }
            $.ajax({
                url: "/guest/signin",
                method: "POST",
                data: {
			_token,
                    phone,
                },
                dataType: "json",
                success: function (data) {
                    document.cookie = `phone=${phone}`;
                    location.href = "/sharing";
                },
                error: function () {
                    swal(
                        {
                            title: "失败！",
                            text: `这个电话号码没有注册！`,
                            type: "error",
                        },
                        function () {
                            button.attr("disabled", false);
                        }
                    );
                },
            });
        } else {
            swal({
                title: "Failed !",
                text: `Phone isn't valid !`,
                type: "error",
            });
            $(this).attr("disabled", false);
        }
    });

    $(".copyBtn").on("click", function () {
        /* Select the text field */
        const copyText = $(this)
            .parent()
            .parent()
            .parent()
            .find(".sharingLink");
        copyText.select();
        /* Copy the text inside the text field */
        document.execCommand("copy");
        swal({
            title: "成功",
            text: `您的分享链接已复制到剪贴板`,
            type: "success",
        });
    });

    $(".sharingLink").on("click", function () {
        $(this).select();
    });

    function drawCanvas(qrSrc, backgroundSrc, qrX, qrY, qrWidth, qrHeight) {
        return new Promise((resolve, reject) => {
            const canvas = document.createElement("canvas");
            const ctx = canvas.getContext("2d");
            const background = new Image();
            background.crossOrigin = "Anonymous";
            background.src = backgroundSrc;
            const qr = new Image();
            qr.crossOrigin = "Anonymous";
            qr.src = qrSrc;
            background.onload = () => {
                qr.onload = () => {
                    canvas.width = background.width;
                    canvas.height = background.height;
                    ctx.drawImage(
                        background,
                        0,
                        0,
                        canvas.width,
                        canvas.height
                    );
                    ctx.drawImage(qr, qrX, qrY, qrWidth, qrHeight);
                    resolve(canvas.toDataURL("image/png"));
                };
            };
        });
    }

    $(".create-invitation-button").on("click", function (e) {
        e.preventDefault();

        $(this).attr("disabled", true);

        const template1 = document.getElementById("template1");
        const template2 = document.getElementById("template2");
        const qrCode = document.getElementsByClassName("qrCode")[0];
        console.log(qrCode);
	drawCanvas(qrCode.src, template1.src, 178, 1472, 250, 250).then(
            (image) => {
                document.getElementById("invitation1").src = image;
                $("#invitation1").parent().attr("href", image);
            }
        );
	drawCanvas(qrCode.src, template2.src, 1068, 2036, 250, 250).then(
            (image) => {
                document.getElementById("invitation2").src = image;
                $("#invitation2").parent().attr("href", image);
            }
        );
        $("#invitationPanel").removeClass("hidden");
    });
    $("#resetBtn").on("click", function () {
        $("#registerForm")[0].reset();
    });
});

function checkPhoneNumber(phone) {
    const phoneCheck = /((133|149|153|173|177|180|181|189|199|130|131|132|145|155|156|166|171|175|176|185|186|166|134|135|136|137|138|139|147|150|151|152|157|158|159|172|178|182|183|184|187|188|198|191)+([0-9]{8})\b)/g;
    return !(phone.length != 11 || !phoneCheck.test(phone));
}

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
