$(function () {
    let ticketStatusHTML = [
        `<button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Registered</button>`,
        `<button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-warning">Collected</button>`,
    ];
    let ticketTypeHTML = [
        `<button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Normal</button>`,
        `<button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-warning">VIP</button>`,
    ];
    ("use strict");
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $(".saveBtn").on("click", function () {
        const button = $(this);
        button.attr("disabled", true);
        let form;
        if (button.data("function") == "update") {
            form = $("#updateForm");
        } else {
            form = $("#infoForm");
        }
        const method = $(this).data("function");

        let formData = form.serializeArray();
        let data = {};

        $(formData).each(function (index, obj) {
            data[obj.name] = obj.value;
        });
        data.id = $(this).data("id");
        data.code = form.find("[name=code]").val();

        $.ajax({
            url: `/admin/tickets/${method}`,
            method: "POST",
            data,
            success: function (data) {
                if (data.duplicated) {
                    if (data.duplicated.length > 0 ) {
                        let duplicatedCode = "";
                        data.duplicated.forEach(function (item) {
                            duplicatedCode += item + ",";
                        });
                        swal(
                            {
                                title: "Completed!",
                                text: `${duplicatedCode} is duplicated !`,
                                type: "warning",
                            },
                            function () {
                                location.reload(true);
                            }
                        );
                    } else
                    swal(
                        {
                            title: "Completed!",
                            text: `Your ticket was ${method}d`,
                            type: "success",
                        },
                        function () {
                            location.reload(true);
                        }
                    );
                } else
                swal(
                    {
                        title: "Completed!",
                        text: `Your ticket was ${method}d`,
                        type: "success",
                    },
                    function () {
                        location.reload(true);
                    }
                );
            },
            error: function (data) {
                swal(
                    {
                        title: "失败！",
                        text: `请检查您的信息，然后再试一次！`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            },
        });
    });

    $("#myTable").on("click", ".deleteBtn", function (e) {

        let button = $(this);
        e.preventDefault();
        let id = button.data("id");
        swal(
            {
                title: "Are you sure?",
                text: "You will not be able to recover this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
            },
            function () {
                $.ajax({
                    url: "/admin/tickets/delete",
                    method: "POST",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        swal(
                            {
                                title: "Completed!",
                                text: "Your customer was deleted",
                                type: "success",
                            },
                            function () {
                                location.reload(true);
                            }
                        );
                    },
                    error: function (data) {
                        console.log(data);
                        swal(
                            {
                                title: "失败！",
                                text: `请检查您的信息，然后再试一次！`,
                                type: "error",
                            },
                            function () {
                                button.attr("disabled", false);
                            }
                        );
                    },
                });
            }
        );
    });

    $("#myTable").on("click", ".editBtn", function () {
        const modal = $("#updateModal");
        const form = $("#updateModal").find("#updateForm");
        const preloader = modal.find(".preloader");
        preloader.css("display", "block");
        const id = $(this).data("id");
        modal.find(".saveBtn").attr("data-id", id);

        $.ajax({
            method: "GET",
            url: `/admin/tickets/get?id=${id}`,
            dataType: "json",
            success: function (data) {
                form.find("[name=code]").val(data.ticket.code);
                form.find("[name=user]").val(data.ticket.user_id);

                preloader.css("display", "none");
            },
            error: function (data) {
                swal(
                    {
                        title: "失败！",
                        text: `请检查您的信息，然后再试一次！`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            },
        });
    });

    // $("#infoModal").on("hidden.bs.modal", function (e) {
    //     console.log("Closed");
    //     location.reload(true);
    // });
    // $("#updateModal").on("hidden.bs.modal", function (e) {
    //     console.log("Closed");
    //     location.reload(true);
    // });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
