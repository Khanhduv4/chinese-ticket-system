$(function () {
    "use strict";
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $("#saveBtn").on("click", function () {
        const button = $(this);
        button.attr("disabled", true);
        const form = $("#infoForm");
        const method = $(this).data("function");

        let formdata = form.serializeArray();
        let data = {};
        $(formdata).each(function (index, obj) {
            data[obj.name] = obj.value;
        });
        data.id = $(this).data("id");
        $.ajax({
            url: `/admin/users/${method}`,
            method: "POST",
            data,
            success: function (data) {
                swal(
                    {
                        title: "Completed!",
                        text: `Your customer was ${method}d`,
                        type: "success",
                    },
                    function () {
                        location.reload(true);
                    }
                );
            },
        });
    });

    $("#myTable").on("click", ".deleteBtn", function (e) {
        let button = $(this);
        e.preventDefault();
        let id = button.data("id");
        swal(
            {
                title: "Are you sure?",
                text: "You will not be able to recover this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
            },
            function () {
                $.ajax({
                    url: "/admin/users/delete",
                    method: "DELETE",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        swal(
                            {
                                title: "Completed!",
                                text: "Your user was deleted",
                                type: "success",
                            },
                            function () {
                                location.reload(true);
                            }
                        );
                    },
                });
            }
        );
    });

    $("#myTable").on("click", ".editBtn", function () {

        const modal = $("#infoModal");
        const form = $("#infoModal").find("#infoForm");
        const preloader = modal.find(".preloader");
        preloader.css("display", "block");
        const id = $(this).data("id");
        modal.find("#saveBtn").attr("data-function", "update");
        modal.find("#saveBtn").attr("data-id", id);

        $.ajax({
            method: "GET",
            url: `/admin/users/get?id=${id}`,
            dataType: "json",
            success: function (data) {
                form.find("[name=name]").val(data.user.name);
                form.find("[name=phone]").val(data.user.phone);
                form.find("[name=code]").val(data.user.code);
                form.find("[name=organization]").val(data.user.organization);
                form.find("[name=address]").val(data.user.address);
                form.find("[name=city]").val(data.user.city.id);
                form.find("[name=role]").val(data.user.role);
                form.find("[name=password]").parent().hide();

                preloader.css("display", "none");
            },
            error: function (data) {
                console.log(data);
                swal(
                    {
                        title: "失败！",
                        text: `请检查您的信息，然后再试一次！`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            },
        });
    });

    $("#xlsx-file").on("change", function (e) {
        const files = e.target.files,
            f = files[0];
        const reader = new FileReader();
        reader.onload = function (e) {
            const inputFile = new Uint8Array(e.target.result);
            const workbook = XLSX.read(inputFile, { type: "array" });
            const first_sheet_name = workbook.SheetNames[0];

            /* Get worksheet */
            const worksheet = workbook.Sheets[first_sheet_name];
            const sheetData = XLSX.utils.sheet_to_row_object_array(worksheet);
            let data = { json: sheetData };

            console.log(data);
            $.ajax({
                url: "/admin/users/import",
                method: "POST",
                data,
                success: function (dataRes) {
                    swal(
                        {
                            title: "Completed!",
                            text: `Import successfully !`,
                            type: "success",
                        },
                        function () {
                            location.reload(true);
                        }
                    );
                },
            });

            /* DO SOMETHING WITH workbook HERE */
        };
        reader.readAsArrayBuffer(f);
    });


    $("#infoModal").find("[name=city]").on("change", function() {
        console.log($(this).val());
        if ($(this).val() == -1) {
            $("#infoModal").find("[name=newCity]").parent().removeClass("hidden-xs-up");
        } else {
            $("#infoModal").find("[name=newCity]").parent().removeClass("hidden-xs-up").addClass("hidden-xs-up");

        }
    })

    $("#infoModal").on("hidden.bs.modal", function (e) {
        console.log("Closed");
        location.reload(true);
    });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
