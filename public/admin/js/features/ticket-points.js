$(function () {
    "use strict";
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $("#infoForm")
        .find("input")
        .not("[type=submit]")
        .jqBootstrapValidation({
            submitSuccess: function ($form, event) {
                event.preventDefault();
                const button = $("#saveBtn");
                button.attr("disabled", true);
                const form = $("#infoForm");
                const method = button.data("function");

                let formdata = form.serializeArray();
                let data = {};
                $(formdata).each(function (index, obj) {
                    data[obj.name] = obj.value;
                });
                data.id = button.data("id");
                $.ajax({
                    url: `/admin/ticket-points/${method}`,
                    method: "POST",
                    data,
                    success: function (data) {
                        swal(
                            {
                                title: "Completed!",
                                text: `Your ticket point was ${method}d`,
                                type: "success",
                            },
                            function () {
                                location.reload(true);
                            }
                        );
                    },
                    error: function (data) {
                        console.log(data);
                        swal(
                            {
                                title: "失败！",
                                text: `请检查您的信息，然后再试一次！`,
                                type: "error",
                            },
                            function () {
                                button.attr("disabled", false);
                            }
                        );
                    },
                });
            },
        });

    $(".deleteBtn").on("click", function (e) {
        let button = $(this);
        e.preventDefault();
        let id = button.data("id");
        swal(
            {
                title: "Are you sure?",
                text: "You will not be able to recover this point!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
            },
            function () {
                $.ajax({
                    url: "/admin/ticket-points/delete",
                    method: "DELETE",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        swal(
                            {
                                title: "Completed!",
                                text: "Your point was deleted",
                                type: "success",
                            },
                            function () {
                                location.reload(true);
                            }
                        );
                    },
                });
            }
        );
    });
    $(".editBtn").on("click", function () {
        const modal = $("#infoModal");
        const form = $("#infoModal").find("#infoForm");
        const preloader = modal.find(".preloader");
        preloader.css("display", "block");
        const id = $(this).data("id");
        modal.find("#saveBtn").attr("data-function", "update");
        modal.find("#saveBtn").attr("data-id", id);

        $.ajax({
            method: "GET",
            url: `/admin/ticket-points/get?id=${id}`,
            dataType: "json",
            success: function (data) {
                form.find("[name=city]").val(data.point.city);
                form.find("[name=address]").val(data.point.address);
                preloader.css("display", "none");
            },
        });
    });

    $("#infoModal").on("hidden.bs.modal", function (e) {
        console.log("Closed");
        location.reload(true);
    });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
