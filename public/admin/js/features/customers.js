// const { forEach } = require("lodash");

$(function () {
    let code = `<button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Ticket Code: </button>`;
    let ticketStatusHTML = [
        `<button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Registered</button>`,
        `<button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-warning">Collected</button>`,
    ];
    let ticketTypeHTML = [
        `<button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Normal</button>`,
        `<button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-warning">VIP</button>`,
    ];
    ("use strict");
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $("#saveBtn").on("click", function () {
        const button = $(this);
        button.attr("disabled", true);
        const form = $("#infoForm");
        const method = $(this).data("function");

        let formdata = form.serializeArray();
        let data = {};
        $(formdata).each(function (index, obj) {
            data[obj.name] = obj.value;
        });
        data.id = $(this).data("id");
        $.ajax({
            url: `/admin/customers/${method}`,
            method: "POST",
            data,
            success: function (data) {
                swal(
                    {
                        title: "Completed!",
                        text: `Your user was ${method}d`,
                        type: "success",
                    },
                    function () {
                        location.reload(true);
                    }
                );
            },
            error: function (data) {
                console.log(data);
                swal(
                    {
                        title: "失败！",
                        text: `请检查您的信息，然后再试一次！`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            },
        });
    });

    $(".deleteBtn").on("click", function (e) {
        let button = $(this);
        e.preventDefault();
        let id = button.data("id");
        swal(
            {
                title: "Are you sure?",
                text: "You will not be able to recover this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
            },
            function () {
                $.ajax({
                    url: "/admin/customers/delete",
                    method: "DELETE",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        swal(
                            {
                                title: "Completed!",
                                text: "Your customer was deleted",
                                type: "success",
                            },
                            function () {
                                location.reload(true);
                            }
                        );
                    },
                    error: function (data) {
                        console.log(data);
                        swal(
                            {
                                title: "失败！",
                                text: `请检查您的信息，然后再试一次！`,
                                type: "error",
                            },
                            function () {
                                button.attr("disabled", false);
                            }
                        );
                    },
                });
            }
        );
    });

    $(".checkBtn").on("click", function (e) {
        let button = $(this);
        e.preventDefault();
        let id = button.data("id");
        swal(
            {
                title: "Are you sure?",
                text: "Give tickets to this customer ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
            },
            function () {
                $.ajax({
                    url: "/admin/customers/giveTicket",
                    method: "POST",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        swal(
                            {
                                title: "Completed!",
                                text: "Ticket given",
                                type: "success",
                            },
                            function () {
                                location.reload(true);
                            }
                        );
                    },
                    error: function (data) {
                        console.log(data);
                        swal(
                            {
                                title: "失败！",
                                text: `请检查您的信息，然后再试一次！`,
                                type: "error",
                            },
                            function () {
                                button.attr("disabled", false);
                            }
                        );
                    },
                });
            }
        );
    });

    $(".editBtn").on("click", function () {
        const modal = $("#infoModal");
        const form = $("#infoModal").find("#infoForm");
        const preloader = modal.find(".preloader");
        preloader.css("display", "block");
        const id = $(this).data("id");
        modal.find("#saveBtn").attr("data-function", "update");
        modal.find("#saveBtn").attr("data-id", id);

        $.ajax({
            method: "GET",
            url: `/admin/users/get?id=${id}`,
            dataType: "json",
            success: function (data) {
                form.find("[name=name]").val(data.user.name);
                form.find("[name=phone]").val(data.user.phone);
                form.find("[name=email]").val(data.user.email);
                form.find("[name=role]").val(data.user.role);
                form.find("[name=password]").parent().hide();

                preloader.css("display", "none");
            },
            error: function (data) {
                console.log(data);
                swal(
                    {
                        title: "失败！",
                        text: `请检查您的信息，然后再试一次！`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            },
        });
    });

    // $("#searchBtn").on("click", function () {

    // });

    $("#searchForm")
        .find("input")
        .not("[type=submit]")
        .jqBootstrapValidation({
            submitSuccess: function ($form, event) {
                event.preventDefault();
                const button = $(this);
                button.attr("disabled", true);
                button.html(`<i class="fa fa-spin fa-circle-o-notch"></i>`);
                const phoneSearch = $("#searchText").val();
                const infoPanel = $("#infoPanel");
                $.ajax({
                    method: "GET",
                    url: `/admin/customers/search?phone=${phoneSearch}`,
                    dataType: "json",
                    success: function (data) {
                        infoPanel.removeClass("hidden-xs-up");
                        infoPanel.find("[name=name]").text(data.customer.name);
                        infoPanel
                            .find("[name=phone]")
                            .text(data.customer.phone);
                        infoPanel
                            .find("[name=organization]")
                            .text(data.customer.organization);
                        infoPanel
                            .find("[name=title]")
                            .text(data.customer.title);
                        let statusBtn =
                            ticketStatusHTML[data.customer.ticket_status];
                        if (data.customer.ticket_status == 0) {
                            statusBtn += `<button type="button" data-id = "${data.customer.id}" data-toggle="modal" data-target="#ticketLeft" class="giveBtn btn waves-effect waves-light btn-sm btn-rounded btn-danger ml-1">Give ticket</button>`;
                        } else {
                            if (data.ticket)
                                statusBtn += `<button type="button" class="btn waves-effect waves-light btn-sm btn-rounded btn-primary">Ticket Code: ${data.ticket.code}</button>`;
                        }
                        infoPanel.find("[name=ticket-status]").html(statusBtn);
                        infoPanel
                            .find("[name=ticket-type]")
                            .html(ticketTypeHTML[data.customer.ticket_type]);
                    },
                    error: function (data) {
                        swal(
                            {
                                title: "失败！",
                                text: `电话号码：${phoneSearch}未注册！`,
                                type: "error",
                            },
                            function () {
                                infoPanel.addClass("hidden-xs-up");
                            }
                        );
                    },
                    complete: function () {
                        button.attr("disabled", false);
                        button.text("Search");
                    },
                }).done(function () {});
            },
        });

    $("#giveTicketBtn").on("click", function (e) {
        let id = $(this).data("id");

        swal(
            {
                title: "Are you sure?",
                text: "Give this ticket ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false,
            },
            function () {
                $.ajax({
                    url: "/admin/customers/giveTicket",
                    method: "POST",
                    data: {
                        ticket_id: $("#ticketLeft").find("[name=ticket]").val(),
                        id: id,
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            swal(
                                {
                                    title: "Error!",
                                    text:
                                        "You have run out of tickets, please contact the admin to get more tickets",
                                    type: "error",
                                },
                                function () {}
                            );
                        } else
                            swal(
                                {
                                    title: "Completed!",
                                    text: "Ticket status was updated",
                                    type: "success",
                                },
                                function () {
                                    location.reload(true);
                                }
                            );
                    },
                    error: function (data) {
                        console.log(data);
                        swal(
                            {
                                title: "失败！",
                                text: `请检查您的信息，然后再试一次！`,
                                type: "error",
                            },
                            function () {
                                // button.attr("disabled", false);
                            }
                        );
                    },
                });
            }
        );
    });

    $("[name=ticket-status]").on("click", ".giveBtn", function (e) {
        e.preventDefault();

        let button = $(this);

        const id = $(this).data("id");

        $("#giveTicketBtn").attr("data-id", id);

        $.ajax({
            url: "/admin/users/get-ticket-left",
            method: "GET",
            data: {
                id: id,
            },
            success: function (data) {
                if (data) {
                    let options = "";
                    data.forEach((element) => {
                        options += `<option value = ${element.id}>${element.code}</option>`;
                    });
                    $("#ticketLeft").find("select").append(options);
                } else
                    swal(
                        {
                            title: "Failed!",
                            text: "Something wrong",
                            type: "error",
                        },
                        function () {
                            location.reload(true);
                        }
                    );
            },
            error: function (data) {
                console.log(data);
                swal(
                    {
                        title: "失败！",
                        text: `请检查您的信息，然后再试一次！`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            },
        });

        return;
    });

    $("#infoModal").on("hidden.bs.modal", function (e) {
        console.log("Closed");
        location.reload(true);
    });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
