$(function () {
    "use strict";
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });

    $("#infoForm")
        .find("input")
        .not("[type=submit]")
        .jqBootstrapValidation({
            submitSuccess: function ($form, event) {
                event.preventDefault();
                const button = $("#saveBtn");
                button.attr("disabled", true);
                const method = button.data("function");

                let formData = $("#infoForm").serializeArray();
                let data = {};
                $(formData).each(function (index, obj) {
                    data[obj.name] = obj.value;
                });
                console.log(data);
                data.body = $(".summernote").summernote("code");
                data.id = button.data("id");
                $.ajax({
                    url: `/admin/posts/${method}`,
                    method: "POST",
                    data,
                    success: function (data) {
                        swal(
                            {
                                title: "Completed!",
                                text: `Your post was ${method}d`,
                                type: "success",
                            },
                            function () {
                                location.href = "/admin/posts";
                            }
                        );
                    },
                    error: function (data) {
                        console.log(data);
                        swal(
                            {
                                title: "失败！",
                                text: `您的帖子无法发布！请检查您的内容！`,
                                type: "error",
                            },
                            function () {
                                button.attr("disabled", false);
                            }
                        );
                    },
                });
            },
        });

    $(".deleteBtn").on("click", function (e) {
        let button = $(this);
        e.preventDefault();
        let id = button.data("id");
        swal(
            {
                title: "Are you sure?",
                text: "You will not be able to recover this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
            },
            function () {
                $.ajax({
                    url: "/admin/posts/delete",
                    method: "DELETE",
                    data: {
                        id: id,
                    },
                    success: function (data) {
                        swal(
                            {
                                title: "Completed!",
                                text: "Your user was deleted",
                                type: "success",
                            },
                            function () {
                                location.reload(true);
                            }
                        );
                    },
                });
            }
        );
    });

    $("#myTable").on("click", ".reviewBtn", function () {
        const modal = $("#infoModal");
        const preloader = modal.find(".preloader");
        preloader.css("display", "block");
        const id = $(this).data("id");
        $("#reviewed-post-id").val(id);
        $.ajax({
            method: "GET",
            url: `/admin/posts/get?id=${id}`,
            dataType: "json",
            success: function (data) {
                $(".post-information")
                    .find(".post-title")
                    .html(
                        `News Feed: ${data.post.news_feed} Media: ${data.post.media} Website: ${data.post.website}`
                    );
                $(".post-information").find(".post-body").html(data.post.body);
                $(".post-information")
                    .find(".post-score")
                    .html(data.post.score);

                preloader.css("display", "none");
            },
            error: function (data) {
                console.log(data);
                swal(
                    {
                        title: "失败！",
                        text: `请检查您的信息，然后再试一次！`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            },
        });
    });

    $(".saveReviewBtn").on("click", function () {
        const button = $(this);
        button.attr("disabled", true);
        const form = $("#reviewForm");
        let formData = form.serializeArray();
        let data = {};
        data.id = $("#reviewed-post-id").val();
        data.status = button.data("status");
        $(formData).each(function (index, obj) {
            data[obj.name] = obj.value;
        });
        $.ajax({
            url: `/admin/posts/review`,
            method: "POST",
            data,
            success: function (data) {
                swal(
                    {
                        title: "Completed!",
                        text: `You have just reviewed the post !`,
                        type: "success",
                    },
                    function () {
                        location.href = "/admin/posts";
                    }
                );
            },
            error: function (data) {
                console.log(data);
                swal(
                    {
                        title: "失败！",
                        text: `请检查您的信息，然后再试一次！`,
                        type: "error",
                    },
                    function () {
                        button.attr("disabled", false);
                    }
                );
            },
        });
    });

    $(".getMessageBtn").on("click", function () {
        const status = $(this).data("status");
        if (status == 1)
            swal({
                title: "Message !",
                text: $(this).data("message"),
                type: "success",
            });
        else
            swal({
                title: "Message !",
                text: $(this).data("message"),
                type: "warning",
            });
    });

    $("#infoModal").on("hidden.bs.modal", function (e) {
        console.log("Closed");
        location.reload(true);
    });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
