$(function () {
    "use strict";

    $("input,select,textarea")
        .not("[type=submit]")
        .jqBootstrapValidation({
            submitSuccess: function ($form, event) {
                event.preventDefault();
                const form = $("#registerForm");
                const infoPanel = $(".infoPanel");
                const button = $("#registerBtn");
                const preloader = $(".preloader");
                preloader.css("display", "block");
                button.attr("disabled", true);
                let formData = form.serializeArray();
                let data = {};
                $(formData).each(function (index, obj) {
                    data[obj.name] = obj.value;
                });
                data.refCode = form.find("#refCode").val();
                console.log(data);
                $.ajax({
                    url: `/register`,
                    method: "POST",
                    data,
                    success: function (data) {
                        infoPanel.find("[name=name]").html(data.guest.name);
                        infoPanel.find("[name=email]").html(data.guest.email);
                        infoPanel.find("[name=phone]").html(data.guest.phone);
                        infoPanel.find("[name=address]").html(data.address);
                        const sharingLink = `https://${window.location.hostname}/ref=${data.guest.ref_code}`;
                        infoPanel
                            .find("#qrCode")
                            .attr(
                                "src",
                                `https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${sharingLink}`
                            );
                        infoPanel
                            .find("#sharingLink")
                            .val(
                                `https://${window.location.hostname}?ref=${data.guest.ref_code}`
                            );

                        form.hide();
                        infoPanel.removeClass("hidden-xs-up");
                    },
                    error: function (data) {
                        console.log(data);
                        swal(
                            {
                                title: "失败！",
                                text: `请检查您的信息，然后再试一次！`,
                                type: "error",
                            },
                            function () {
                                button.attr("disabled", false);
                            }
                        );
                    },
                    complete: function (data) {
                        preloader.css("display", "none");
                    },
                });
            },
        });
    $("#copyBtn").on("click", function () {
        /* Select the text field */
        const copyText = $("#sharingLink");
        copyText.select();
        /* Copy the text inside the text field */
        document.execCommand("copy");
        swal({
            title: "Completed !",
            text: `Your link was copied to your clipboard`,
            type: "success",
        });
    });
    $("#sharingLink").on("click", function () {
        $(this).select();
    });
    $("#resetBtn").on("click", function () {
        $("#registerForm")[0].reset();
    });
});

String.prototype.replaceAll = function (f, r) {
    return this.split(f).join(r);
};
