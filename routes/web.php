<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// --------------------------------------------
// CLIENT-ROUTE
// --------------------------------------------

//test view
Route::get('/testview', function () {
    return view('admin.managements.dashboard');
});

Route::get('/', 'CustomerController@viewClient')->name("guest-home");
Route::get('/center', 'CustomerController@viewCenter')->name("guest-center");
Route::get('/signin', 'CustomerController@viewSignIn')->name("guest-signin");
Route::get('/sharing', 'CustomerController@viewSharing')->name("guest-sharing");

Route::post('/guest/signin', 'CustomerController@signIn');
Route::post('/register', 'CustomerController@create');
Route::post('/guest/getOTP', 'CustomerController@getOTP');
// --------------------------------------------
// END CLIENT-ROUTE
// --------------------------------------------

// --------------------------------------------
// ADMIN-ROUTE
// --------------------------------------------


Route::get('/errors/403', function () {
    return view('errors.403');
});
Route::prefix('admin')->group(function () {

	 Route::middleware('auth')->group(function () {
        //view
        Route::get('/ranked', 'UserController@viewRanked');
        Route::get('/point', 'CustomerController@point');

        Route::get('/ticket-points', 'TicketPointController@view');
        Route::get('/posts', 'PostController@view');
        Route::get('/posts/create', 'PostController@viewCreator');
        Route::get('/posts/edit/{id}', 'PostController@viewCreator');

        Route::get('/customers', 'CustomerController@view');
        Route::post('/customers/create', 'CustomerController@create');
        Route::post('/customers/giveTicket', 'CustomerController@giveTicket');
        Route::delete('/customers/delete', 'CustomerController@delete');
        Route::get('/customers/search', 'CustomerController@search');

        Route::post('/changePassword', 'UserController@changePassword');

        //CRUD
        // user
        Route::get('/users/get-ticket-left', 'UserController@getTicketLeft');

        //ticket point
        Route::get('/ticket-points/get', 'TicketPointController@getById');
        Route::post('/ticket-points/create', 'TicketPointController@create');
        Route::post('/ticket-points/update', 'TicketPointController@update');
        Route::delete('/ticket-points/delete', 'TicketPointController@delete');
        //post
        Route::get('/posts/get', 'PostController@getById');
        Route::post('/posts/create', 'PostController@create');
        Route::post('/posts/update', 'PostController@update');
        Route::delete('/posts/delete', 'PostController@delete');
    });

  //  Route::middleware('CheckAdmin')->group(function () {
        Route::get('/dashboard', "DashboardController@view");

        Route::get('/users', 'UserController@view');
        Route::get('/tickets', 'TicketController@view');
        Route::get('/tickets/get', 'TicketController@getById');
        Route::post('/tickets/create', 'TicketController@create');
        Route::post('/tickets/update', 'TicketController@update');
        Route::post('/tickets/delete', 'TicketController@delete');

        Route::post('/users/import', 'UserController@importXls');

        Route::get('/users/get', 'UserController@getById');
        Route::post('/users/create', 'UserController@create');
        Route::post('/users/update', 'UserController@update');
        Route::delete('/users/delete', 'UserController@delete');
        Route::post('/posts/review', 'PostController@review');
   // });
});

// --------------------------------------------
// END ADMIN-ROUTE
// --------------------------------------------

// --------------------------------------------
// AUTH-ROUTE
// --------------------------------------------
Route::post('login', 'AuthController@authenticate')->name('login');
Route::get('login', 'AuthController@view');
Route::get('logout', 'AuthController@logout')->name('logout');


// --------------------------------------------
// END AUTH-ROUTE
// --------------------------------------------
