<?php $__env->startSection("styles"); ?>
<style>
    nav.navbar.bootsnav.no-background.white .attr-nav>ul>li>a,
    nav.navbar.bootsnav.navbar-transparent.white .attr-nav>ul>li>a,
    nav.navbar.bootsnav.navbar-transparent.white ul.nav>li>a,
    nav.navbar.bootsnav.no-background.white ul.nav>li>a {
        color: grey;
    }
    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
        background-color: #f90048;
    }
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<!--page title start-->
<section class="ImageBackground ImageBackground--overlay v-align-parent u-height350 u-BoxShadow40" data-overlay="0">
    <div class="ImageBackground__holder"
        style="background-image: url(<?php echo e(asset('guest/assets/imgs/banner/inner-banner.jpg')); ?>);">
        <img src="<?php echo e(asset('guest/assets/imgs/banner/inner-banner.jpg')); ?>" alt="">
    </div>
    <div class="v-align-child">
        <div class="container ">
            <div class="row ">
                <div class="col-md-12 text-center">
                    <h1 class="text-uppercase u-Margin0 u-Weight700">领票点查询</h1>
                </div>
            </div>
        </div>
    </div>
</section>
<!--page title end-->


<div class="container py-5">
    <ul id="myTabs" class="nav nav-pills mb-3" role="tablist" data-tabs="tabs">
        <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if($loop->first): ?>
        <li class="active"><a href="#pills-<?php echo e($city->id); ?>" data-toggle="tab"><?php echo e($city->name); ?></a></li>

        <?php else: ?>
        <li><a href="#pills-<?php echo e($city->id); ?>" data-toggle="tab"><?php echo e($city->name); ?></a></li>

        <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
    <div class="tab-content">
        <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div role="tabpanel" class="tab-pane fade <?php if($loop->first): ?> in active <?php endif; ?>" id="pills-<?php echo e($city->id); ?>">
            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <!-- 1st day start -->
            <?php if($user->city_id == $city->id): ?>
            <div class="row mb-3">
                <div class="col-md-3 pl-md-5 pt-2">
                    <div class="mb-2">
                        <p><i class="fa fa-user-circle mr-2"></i>
                            <?php echo e($user->name); ?></p>
                    </div>
                    <div class="">
                        <p><i class="fa fa-phone-square mr-2"></i>
                            <?php echo e($user->phone); ?></p>
                    </div>
                </div>
                <div class="col-md-7 mt-4 mt-md-0">
                    <h4 class="my-1"><?php echo e($user->code); ?> | <?php echo e($user->organization); ?></h4>
                    <p><em><?php echo e($city->name); ?></em></p>
                    <h6 class="mt-1 mb-1"><?php echo e($user->address); ?></h6>
                </div>
                <div class="col-md-2 pt-2">
                    <h6>剩余门票: <span class="ticket-left"><?php echo e($user->ticket_left); ?></span></h6>
                </div>
            </div>

            <hr>
            <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('guest.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /www/wwwroot/pw.zzfexpo.gk18.cn/resources/views/guest/center.blade.php ENDPATH**/ ?>