<?php
	$status = ['<button type="button" class="btn btn-xs waves-effect waves-light btn-rounded btn-warning">Pending</button>', '<button type="button" class="btn btn-xs waves-effect waves-light btn-rounded btn-primary">Reviewed</button>']
?>


<?php $__env->startSection('styles'); ?>
<style>
	.post-information img {
		max-width:100%!important;
	}

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Activities Management</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
				<li class="breadcrumb-item active">Posts</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row p-3">
							<div class="col-md-8">
								<h3 class="card-title">Activities List</h3>
							</div>
							<div class="col-md-4">
								<a href="/admin/posts/create">
									<button type="button" class="btn waves-effect waves-light btn-primary pull-right">Create resources</button>
								</a>
							</div>
							<!-- sample modal content -->
							<div class="modal fade" id ="infoModal" tabindex="-1" role="dialog" aria-labelledby="postInfoModalLabel" aria-hidden="true" style="display: none;">
								<div class="modal-dialog modal-lg">
									<div class="preloader">
										<svg class="circular" viewbox="25 25 50 50">
											<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
									</div>
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title" id="myLargeModalLabel">Review activity</h4>
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
										</div>
										<div class="modal-body">
											<div class="card card-outline-info">
												<div class="card-header">
													<h4 class="m-b-0 text-white">Activity Infomation</h4></div>
												<div class="card-body post-information">
													<div class="row">
														<div class="col-6 b-r"> <strong>Title</strong>
															<br>
															<p class="text-muted post-title" name = "title"></p>
														</div>
														<div class="col-6"> <strong>Score</strong>
															<br>
															<p class="text-muted post-score" name = "score"></p>
														</div>
													</div>
													<hr>
													<div class="row">
														<div class="col-12 b-r">
															<div class = "mb-3">
																<strong>Activity content</strong>
															</div>
															<div class="post-body">
															</div>
														</div>

													</div>
												</div>
											</div>
											<div class="card card-outline-info">
												<div class="card-header">
													<h4 class="m-b-0 text-white">Review</h4></div>
												<div class="card-body poster-information">
													<form id = "reviewForm" class="form-horizontal row" novalidate>
														<input id = "reviewed-post-id" type="hidden" value="">
														<div class="form-group col-md-12">
															<label>Message</label>
															<textarea name = "message" class="form-control" rows="5"></textarea>
														</div>
													</form>
											
												</div>
											</div>
										</div>

										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button data-status = 2 type="button" class="saveReviewBtn btn btn-danger">Disapprove</button>

											<button data-status = 1 type="button" class="saveReviewBtn btn btn-success">Approve</button>
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
						</div>
						<div class="table-responsive p-1">
							<table id ="myTable" class="table table-bordered w-auto">
								<thead>
									<tr>
										<th class = "text-center">#</th>
										<th>Title</th>
										<?php if(Auth::user()->role == 0): ?> 
										<th class = "text-center">Posted By</th>
										<?php endif; ?>
										<th class = "text-center">Status</th>
										<th class = "text-center">Score</th>
										<th class = "text-center">Action</th>
										<th class = "text-right">Created At</th>
										<th class = "text-right">Update At</th>
									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td class = "text-center"><?php echo e($loop->index + 1); ?></td>
										<td>
											Newsfeed: <?php echo e($post->news_feed); ?> Media: <?php echo e($post->media); ?> Website: <?php echo e($post->website); ?>

										</td>
										<?php if(Auth::user()->role == 0): ?> 
										<td data-toggle="tooltip" title="Address: <?php echo e($post->user->address); ?>" class = "text-center">
											<?php echo e($post->user->name); ?>

										</td>
										<?php endif; ?>
										<td class = "text-center">
											<?php switch($post->status):
											case (Constants::POST_STATUS_PENDING): ?>
											<button type="button" class="btn btn-xs waves-effect waves-light btn-rounded btn-warning">Pending</button>
											<?php break; ?>
											<?php case (Constants::POST_STATUS_APPROVED): ?>
											<button class="getMessageBtn btn btn-xs waves-effect waves-light btn-rounded btn-primary">Approved</button>
											<?php break; ?>
											<?php case (Constants::POST_STATUS_DISAPPROVED): ?>
											<button type="button" class="btn btn-xs waves-effect waves-light btn-rounded btn-danger">Disapproved</button>
											<?php break; ?>
											<?php default: ?>
												Khanhs
											<?php endswitch; ?>
										</td>
										<td class = "text-center">
											<?php echo e($post->score); ?>

										</td>
										<td class = "text-center product-action">
											<?php if(Auth::user()->role == 0): ?>
											<i data-id = <?php echo e($post->id); ?> data-toggle="modal" data-target="#infoModal" class="reviewBtn fa fa-check text-info m-r-10"></i>
											<?php endif; ?>
											<?php if($post->status != Constants::POST_STATUS_PENDING): ?>
											<i data-message = "<?php echo e($post->message); ?>" data-status = "<?php echo e($post->status); ?>" class="getMessageBtn fa fa-comment text-success m-r-10"></i>
											<?php endif; ?>
											<?php if($post->status != Constants::POST_STATUS_APPROVED && Auth::user()->role != 0): ?>
											<a href="/admin/posts/edit/<?php echo e($post->id); ?>"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
											<a href="" data-id = <?php echo e($post->id); ?> class = "deleteBtn"> <i class="fa fa-close text-danger"></i> </a>
											<?php endif; ?>
										</td>
                                        <td class = "text-right">
                                            <?php echo e(date('H:i:s d/m/Y', strtotime($post->created_at))); ?>

                                        </td>
                                        <td class = "text-right">
                                            <?php echo e(date('H:i:s d/m/Y', strtotime($post->updated_at))); ?>

                                        </td>
						
									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script src = "<?php echo e(asset("admin/js/features/posts.js")); ?>"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /www/wwwroot/pw.zzfexpo.gk18.cn/resources/views/admin/managements/posts.blade.php ENDPATH**/ ?>