<?php
    $role = Auth::user()->role;
?>
<!DOCTYPE html>
<html lang="en">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset("admin/images/favicon.png")); ?>" />

    <title>GK Event System</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo e(asset("admin/plugins/bootstrap/css/bootstrap.min.css")); ?>" rel="stylesheet" />
    <?php echo $__env->yieldContent('styles'); ?>
    <!-- Custom CSS -->
    <link href="<?php echo e(asset("admin/css/style.css")); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset("admin/plugins/sweetalert/sweetalert.css")); ?>" rel="stylesheet" type="text/css" />
    <!-- You can change the theme colors from here -->
    <link href="<?php echo e(asset("admin/css/colors/blue.css")); ?>" id="theme" rel="stylesheet" />
    <style>
        .sidebar-nav>ul>li>a.active {
            background: white!important;
        }
        .yt-wrapper.yt-wrapper_align_right {
            display: none;
        }
        .yt-listbox__input + span {
            display: none!important;
        }
        .yt-listbox__input[value="zh"] + span {
            display: block!important;
        }
    </style>

</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewbox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand p-2" style ="width: 195px; height: auto; background: transparent;" href="/admin/dashboard">
                        <img src="<?php echo e(asset('admin/images/logo.png')); ?>" class = "img-fluid" alt="">
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse justify-content-end">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- Profile -->
                        <!-- ============================================================== -->
                        <li class= "nav-item d-flex align-items-center pr-5">
                            <div id="ytWidget"></div><script src="https://translate.yandex.net/website-widget/v1/widget.js?widgetId=ytWidget&pageLang=en&widgetTheme=light&autoMode=false" type="text/javascript"></script>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo e(asset("admin/images/users/1.jpg")); ?>" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">
                                    <li>
                                        <div class="dw-user-box">
                                            <div class="u-img"><img src="<?php echo e(asset("admin/images/users/1.jpg")); ?>" alt="user" /></div>
                                            <div class="u-text">
                                                <h4><?php echo e(Auth::user()->name); ?></h4>
                                                <p class="text-muted"><?php echo e(Auth::user()->email); ?></p><a data-toggle="modal" data-target="#changePasswordModal" class="btn btn-rounded btn-danger btn-sm">Change Password</a></div>
                                        </div>
                                    </li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- User profile -->
                <div class="user-profile">
                    <!-- User profile image -->
                    <div class="profile-img"> <img src="<?php echo e(asset("admin/images/users/profile.png")); ?>" alt="user" />
                        <!-- this is blinking heartbit-->
                        <div class="notify setpos"> <span class="heartbit"></span> <span class="point"></span> </div>
                    </div>
                    <!-- User profile text-->
                    <div class="profile-text">
                        <h5><?php echo e(Auth::user()->name); ?></h5>
                        <a href="#" class="dropdown-toggle u-dropdown" role="button" data-toggle="modal" data-target="#changePasswordModal" aria-haspopup="true" aria-expanded="true"><i class="mdi mdi-key"></i></a>
                        <a href="/logout" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
                    </div>
                </div>
                <!-- End User profile text-->
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-small-cap">MANAGEMENT</li>
                        <?php if($role == 0): ?>
                        <li> <a class="waves-effect waves-dark" href="/admin/dashboard" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard</span></a>
                        <li> <a class="waves-effect waves-dark" href="/admin/users" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Users</span></a>
                            <li> <a class="waves-effect waves-dark" href="/admin/tickets" aria-expanded="false"><i class="mdi mdi-ticket"></i><span class="hide-menu">Tickets</span></a>
                        </li>

                        <?php endif; ?>
                        <li> <a class="waves-effect waves-dark" href="/admin/customers" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">Customers</span></a>

                        </li>
                        <li> <a class="waves-effect waves-dark" href="/admin/posts" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Activities</span></a>

                        </li>

                        <li> <a class="waves-effect waves-dark" href="/admin/ranked" aria-expanded="false"><i class="mdi mdi-bullseye"></i><span class="hide-menu">Ranked</span></a>

                        </li>



                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <?php echo $__env->yieldContent('content'); ?>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © Develop by KhanhDu
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>

    <div id="changePasswordModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="changePasswordModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Change Password</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <form id = "passwordChangeForm" method="post" novalidate>
                        <div class="form-group">
                            <label>New Password</label>
                            <input type="password" id = "newPassword" class="form-control" placeholder="Enter your password here" required="" data-validation-required-message=
                            "Please enter your password"/>
                            <p class="help-block"></p>
                        </div>


                    </form>
                </div>
                <div class="modal-footer">
                    <button type="submit" form = "passwordChangeForm" id = "changePasswordBtn" class="btn btn-info waves-effect" >Save</button>
                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo e(asset("admin/plugins/jquery/jquery.min.js")); ?>"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo e(asset("admin/plugins/bootstrap/js/popper.min.js")); ?>"></script>
    <script src="<?php echo e(asset("admin/plugins/bootstrap/js/bootstrap.min.js")); ?>"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo e(asset("admin/js/jquery.slimscroll.js")); ?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo e(asset("admin/js/waves.js")); ?>"></script>
    <!--Menu sidebar -->
    <script src="<?php echo e(asset("admin/js/sidebarmenu.js")); ?>"></script>
    <!--stickey kit -->
    <script src="<?php echo e(asset("admin/plugins/sticky-kit-master/dist/sticky-kit.min.js")); ?>"></script>
    <script src="<?php echo e(asset("admin/plugins/sparkline/jquery.sparkline.min.js")); ?>"></script>
    <!--stickey kit -->
    <script src="<?php echo e(asset("admin/plugins/sticky-kit-master/dist/sticky-kit.min.js")); ?>"></script>
    <script src="<?php echo e(asset("admin/plugins/sparkline/jquery.sparkline.min.js")); ?>"></script>
    <script src="<?php echo e(asset("admin/plugins/sweetalert/sweetalert.min.js")); ?>"></script>


    <!--Custom JavaScript -->
    <script src="<?php echo e(asset("admin/js/custom.min.js")); ?>"></script>
    <script src="<?php echo e(asset("admin/js/validation.js")); ?>"></script>
    <script src="<?php echo e(asset("admin/plugins/datatables/jquery.dataTables.min.js")); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.4/js/buttons.html5.min.js"></script>

    <script>
        $(document).ready(function() {
            $('#myTable').DataTable({
                dom: 'Bfrtip',
                buttons: [
            'copy', 'csv', 'excel', 'pdf'
        ]
            });
        });
    </script>

    <script src="<?php echo e(asset("admin/js/features/changePassword.js")); ?>"></script>


    <?php echo $__env->yieldContent('scripts'); ?>

</body>

</html>
<?php /**PATH /www/wwwroot/pw.zzfexpo.gk18.cn/resources/views/admin/layouts/master.blade.php ENDPATH**/ ?>