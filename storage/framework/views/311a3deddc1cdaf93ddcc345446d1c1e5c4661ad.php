<?php
	$status = ['<span class="badge badge-warning">Registered</span>','<span class="badge badge-success">Received</span>'];
?>


<?php $__env->startSection('content'); ?>

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Tickets Management</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
				<li class="breadcrumb-item active">Tickets</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">

						<div class="row p-3">

							<div class="col-md-8">
								<h3 class="card-title">Tickets List</h3>
							</div>
							<div class="col-md-4">
								<button type="button" class="btn waves-effect waves-light btn-primary pull-right" data-toggle="modal" data-target="#infoModal">Create Ticket</button>
								<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
									<div class="modal-dialog" role="document">
										<div class="preloader">
											<svg class="circular" viewbox="25 25 50 50">
												<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
										</div>
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="exampleModalLabel1">Ticket Info</h4>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											</div>
											<div class="modal-body">
												<form class = "form-material p-1" id = "infoForm">
													<div class="row">
														<div class="form-group col-6">
															<label for="code" class="control-label">Ticket Code:</label>
															<input type="number" placeholder="Start" class="form-control" name = "start" />
														</div>
														<div class="form-group col-6">
															<label for="code" class="control-label">‎‎</label>
															<input type="number" placeholder="End" class="form-control" name = "end" />
														</div>
													</div>
													<div class="form-group">
														<label for="ticket-point" class="control-label">User:</label>
														<select name = "user" class="form-control">
															<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<option value="<?php echo e($user->id); ?>"> <?php echo e($user->code); ?> | <?php echo e($user->organization); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</select>
													</div>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" data-function = "create" class="btn btn-primary saveBtn">Save</button>
											</div>
										</div>
									</div>
								</div>
								<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel">
									<div class="modal-dialog" role="document">
										<div class="preloader">
											<svg class="circular" viewbox="25 25 50 50">
												<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
										</div>
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="exampleModalLabel1">Ticket Info</h4>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											</div>
											<div class="modal-body">
												<form class = "form-material p-1" id = "updateForm">

													<div class="form-group">
														<label for="code" class="control-label">Ticket Code:</label>
														<input type="number" disabled placeholder="Example: 120" class="form-control" name = "code" />
													</div>


													<div class="form-group">
														<label for="ticket-point" class="control-label">User:</label>
														<select name = "user" class="form-control">
															<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																<option value="<?php echo e($user->id); ?>"><?php echo e($user->code); ?> | <?php echo e($user->organization); ?></option>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</select>
													</div>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" data-function = "update" class="btn btn-primary saveBtn">Save</button>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="table-responsive p-1">
							<table id ="myTable" class="table table-bordered">
								<thead>
									<tr>
										<th class = "text-center">Ticket code</th>
										<th class = "text-right">Status</th>
										<th class = "text-right">User</th>
										<th class = "text-right">Action</th>
										<th class = "text-right">Created At</th>
										<th class = "text-right">Update At</th>
									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $tickets; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ticket): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
											<td class = "text-center"><?php echo e($ticket->code ?? ""); ?></td>
											<td class = "text-right">
												<?php if($ticket->guest_id): ?>
												Delivered
												<?php else: ?>
												Available
												<?php endif; ?>
											</td>
											<td class = "text-right"><?php echo e($ticket->user->organization ?? ""); ?></td>
											<td class = "text-right">
												<a href="" data-id = <?php echo e($ticket->id ?? ""); ?> class = "editBtn" data-toggle="modal" data-target="#updateModal"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
												<a href="" data-id = <?php echo e($ticket->id ?? ""); ?> class = "deleteBtn" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>

											</td>
											<td class = "text-right"><?php echo e($ticket->created_at ?? ""); ?></td>
											<td class = "text-right"><?php echo e($ticket->updated_at ?? ""); ?></td>
										</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

								</tbody>
							</table>
						</div>


					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src = "<?php echo e(asset("admin/js/features/tickets.js")); ?>"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /www/wwwroot/pw.zzfexpo.gk18.cn/resources/views/admin/managements/tickets.blade.php ENDPATH**/ ?>