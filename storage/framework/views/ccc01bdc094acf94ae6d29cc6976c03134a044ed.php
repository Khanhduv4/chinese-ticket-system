<?php $__env->startSection('styles'); ?>
<link rel="stylesheet" href="<?php echo e(asset('admin/plugins/dropify/dist/css/dropify.min.css')); ?>" />

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Users Management</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
				<li class="breadcrumb-item active">Users</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row p-3">
							<div class="col-md-8">
								<h3 class="card-title">Users List</h3>
							</div>
							<div class="col-md-4">

								<button type="button" class="btn waves-effect waves-light btn-primary pull-right ml-3" data-toggle="modal" data-target="#infoModal">Create User</button>

								<button type="button" class="btn waves-effect waves-light btn-primary pull-right ml-3" data-toggle="modal" data-target="#fileInput">Import XLS</button>

								<div class="modal fade" id="fileInput" tabindex="-1" role="dialog" aria-labelledby="fileInput">
									<div class="modal-dialog" role="document">
										<div class="preloader">
											<svg class="circular" viewbox="25 25 50 50">
												<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
										</div>
										<div class="modal-content">
											<div class="card mb-0">
												<div class="card-body">
													<h4 class="card-title">XLSX file upload</h4>
													<input type="file" id="xlsx-file" class="dropify" data-show-remove="false" />
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
									<div class="modal-dialog" role="document">
										<div class="preloader">
											<svg class="circular" viewbox="25 25 50 50">
												<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
										</div>
										<div class="modal-content">
											<div class="modal-header">
												<h4 class="modal-title" id="exampleModalLabel1">User Info</h4>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											</div>
											<div class="modal-body">
												<form class = "form-material p-1" id = "infoForm">
													<div class="form-group">
														<label for="name" class="control-label">Name:</label>
														<input type="text" placeholder="Example: Tran A" class="form-control" name = "name" />
													</div>
													<div class="form-group">
														<label for="code" class="control-label">Code:</label>
														<input type="text" placeholder="Example: A093" class="form-control" name = "code" />
													</div>
													<div class="form-group">
														<label for="organization" class="control-label">Organization:</label>
														<input type="text" placeholder="Example: TIGVIET" class="form-control" name = "organization" />
													</div>
													<div class="form-group">
														<label for="phone" class="control-label">Phone Number:</label>
														<input type="phone" placeholder="Example: 87128378122" class="form-control" name = "phone" />
													</div>
													<div class="form-group">
														<label for="short-description" class="control-label">Password:</label>
														<input type="password" placeholder="*********" class="form-control" name = "password" />
                                                    </div>
                                                    <div class="form-group">
														<label for="address" class="control-label">Address:</label>
														<input type="text" placeholder="" class="form-control" name = "address" />
                                                    </div>
                                                    <div class="form-group">
														<label for="address" class="control-label">City:</label>
														<select class="form-control" name = "city">
                                                            <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <option value="<?php echo e($city->id); ?>"><?php echo e($city->name); ?></option>
                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <option value = "-1">Other</option>

														</select>
                                                    </div>

                                                    <div class="form-group hidden-xs-up">
														<input type="text" placeholder="Enter your city name" class="form-control" name = "newCity" />
                                                    </div>
													<div class="form-group">
														<label for="role" class="control-label">Role:</label>

														<select name = "role" class="form-control">
															<option value = 0>Administrator</option>
															<option value = 1>Affiliater</option>
														</option></select>
													</div>
												</form>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button" data-function = "create" id = "saveBtn" class="btn btn-primary">Save</button>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
						<div class="table-responsive p-1">
							<table id = "myTable" class="table table-bordered">
								<thead>
									<tr>
										<th class = "text-center">#</th>
										<th>Code</th>
										<th>Name</th>
										<th class = "text-right">Phone</th>
										
										<th class = "text-right">Organization</th>
                                        <th class = "text-right">Address</th>
                                        <th>Score</th>
										<th class = "text-right">Action</th>
										<th class = "text-right">Created At</th>
										<th class = "text-right">Update At</th>
									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td class = "text-center"><?php echo e($loop->index + 1); ?></td>
										<td>
											<?php echo e($user->code); ?>

										</td>
										<td>
											<?php echo e($user->name); ?>

										</td>
										<td class = "text-right">
											<?php echo e($user->phone); ?>

										</td>

										<td class = "text-right">
											<?php echo e($user->organization); ?>

										</td>
										<td class = "text-right">
											<?php echo e($user->address); ?>

                                        </td>
                                        <td><?php echo e($user->score + $user->scoreFromPost); ?></td>
										<td class = "text-right product-action">
											<a href="" data-id = <?php echo e($user->id); ?> class = "editBtn" data-toggle="modal" data-target="#infoModal"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
											<a href="" data-id = <?php echo e($user->id); ?> class = "deleteBtn" data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>
										</td>
                                        <td class = "text-right">
                                            <?php echo e(date('H:i:s d/m/Y', strtotime($user->created_at))); ?>

                                        </td>
                                        <td class = "text-right">
                                            <?php echo e(date('H:i:s d/m/Y', strtotime($user->updated_at))); ?>

                                        </td>


									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src = "<?php echo e(asset("admin/js/features/users.js")); ?>"></script>
<script src = "<?php echo e(asset("admin/plugins/dropify/dist/js/dropify.min.js")); ?>"></script>

<script src = "<?php echo e(asset("admin/js/xlsx.min.js")); ?>"></script>
<script>

	$('.dropify').dropify();
	        // Used events
			var drEvent = $('#input-file-events').dropify();

drEvent.on('dropify.beforeClear', function(event, element) {
	return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
});

drEvent.on('dropify.afterClear', function(event, element) {
	alert('File deleted');
});

drEvent.on('dropify.errors', function(event, element) {
	console.log('Has Errors');
});

var drDestroy = $('#input-file-to-destroy').dropify();
drDestroy = drDestroy.data('dropify')
$('#toggleDropify').on('click', function(e) {
	e.preventDefault();
	if (drDestroy.isDropified()) {
		drDestroy.destroy();
	} else {
		drDestroy.init();
	}
})
</script>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /www/wwwroot/pw.zzfexpo.gk18.cn/resources/views/admin/managements/users.blade.php ENDPATH**/ ?>