<?php
	$status = ['<span class="badge badge-warning">Registered</span>','<span class="badge badge-success">Received</span>'];
?>


<?php $__env->startSection('content'); ?>

	<!-- ============================================================== -->
	<!-- Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<div class="row page-titles">
		<div class="col-md-5 align-self-center">
			<h3 class="text-themecolor">Ranked</h3>
		</div>
		<div class="col-md-7 align-self-center">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="javascript:void(0)">Management</a></li>
				<li class="breadcrumb-item active">Ranked</li>
			</ol>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End Bread crumb and right sidebar toggle -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">
		<!-- ============================================================== -->
		<!-- Start Page Content -->
		<!-- ============================================================== -->
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body">
						<div class="row p-3">

							<div class="col-md-8">
								<h3 class="card-title">Ranked</h3>
							</div>
							<div class="col-md-4">

							</div>

						</div>
						<div class="table-responsive p-1">
							<table id ="myTable" class="table table-bordered">
								<thead>
									<tr>
										<th class = "text-center">#</th>
										<th>Organization</th>
										<th class = "text-center">Score</th>
									</tr>
								</thead>
								<tbody>
									<?php $__currentLoopData = $ranked; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td class = "text-center"><?php echo e($loop->index + 1); ?></td>
										<td>
											<?php echo e($user->organization); ?>

										</td>
										<td class = "text-center">
											<?php echo e($user->total_score); ?>

										</td>
									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- ============================================================== -->
		<!-- End PAge Content -->
		<!-- ============================================================== -->
    </div>
	<!-- ============================================================== -->
	<!-- End Container fluid  -->
	<!-- ============================================================== -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src = "<?php echo e(asset("admin/js/features/customers.js")); ?>"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /www/wwwroot/pw.zzfexpo.gk18.cn/resources/views/admin/managements/ranked.blade.php ENDPATH**/ ?>