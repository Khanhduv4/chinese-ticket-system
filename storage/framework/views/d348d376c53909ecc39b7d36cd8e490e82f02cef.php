<?php $__env->startSection("content"); ?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
	<div class="col-md-5 align-self-center">
		<h3 class="text-themecolor">Dashboard</h3>
	</div>
	<div class="col-md-7 align-self-center">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
			<li class="breadcrumb-item active">Dashboard</li>
		</ol>
	</div>
	<div>
		<button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
	</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Row -->
	<!-- Row -->
	<div class="row">
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<!-- Row -->
					<div class="row">
						<div class="col-8"><h2><?php echo e($totalGuest ?? 0); ?></h2>
							<h6>Registered guests</h6></div>
						<div class="col-4 align-self-center text-right  p-l-0">
							<div id="sparklinedash3"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<!-- Row -->
					<div class="row">
						<div class="col-8"><h2 class=""><?php echo e($collectedTicket ?? 0); ?> <i class="ti-angle-up font-14 text-success"></i></h2>
							<h6>Collected tickets</h6></div>
						<div class="col-4 align-self-center text-right p-l-0">
							<div id="sparklinedash"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<!-- Row -->
					<div class="row">
						<div class="col-8"><h2><?php echo e($vipGuest ?? 0); ?> <i class="ti-angle-up font-14 text-success"></i></h2>
							<h6>VIP Tickets</h6></div>
						<div class="col-4 align-self-center text-right p-l-0">
							<div id="sparklinedash2"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<!-- Row -->
					<div class="row">
						<div class="col-8"><h2><?php echo e($ticketPoints); ?> <i class="ti-angle-down font-14 text-danger"></i></h2>
							<h6>Ticket Points</h6></div>
						<div class="col-4 align-self-center text-right p-l-0">
							<div id="sparklinedash4"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Row -->
	
	<div class="card">
		<div class="card-body">
			<h4 class="card-title">User statistics</h4>
			<h6 class="card-subtitle">User statistics table</h6>
			<div class="table-responsive m-t-40">
				<table id="myTable" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Name</th>
							<th>Organization</th>
							<th>Available Ticket</th>
							<th>Collected tickets</th>
							<th>VIP Tickets</th>
							<th>Total Score</th>
						</tr>
					</thead>
					<tbody>
						<?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						<tr>
							<td><?php echo e($user->name); ?></td>
							<td><?php echo e($user->organization); ?></td>
							<td><?php echo e($user->ticket_left); ?></td>
							<td><?php echo e($user->collected_ticket); ?></td>
							<td><?php echo e($user->vip_ticket); ?></td>
							<td><?php echo e($user->score); ?></td>
						</tr>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Row -->
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->

    <script src="<?php echo e(asset("admin/plugins/sparkline/jquery.sparkline.min.js")); ?>"></script>

    <script src="<?php echo e(asset("admin/js/dashboard4.js")); ?>"></script>


	
    <script src="<?php echo e(asset("admin/js/dashboard2.js")); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make("admin.layouts.master", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /www/wwwroot/pw.zzfexpo.gk18.cn/resources/views/admin/managements/dashboard.blade.php ENDPATH**/ ?>