 <?php $__env->startSection('content'); ?>

<!--hero section start-->
<section
    class="ImageBackground ImageBackground--overlay u-height700- js-Parallax fullscreen"
    data-overlay="5"
>
    <div class="ImageBackground__holder">
        <img src="<?php echo e(asset('guest/assets/imgs/banner/ban1.jpg')); ?>" alt="" />
    </div>
    <div
        class="container u-vCenter- u-PaddingTop100 u-sm-PaddingTop30 u-PaddingBottom100 u-sm-PaddingBottom30"
    >
        <div
            id="registerPanel"
            class="row text-left u-PaddingTop100 u-sm-PaddingTop30 u-PaddingBottom100 u-sm-PaddingBottom30"
        >
            <div class="col-md-7 text-white">
                <h2
                    class="u-FontSize75 u-xs-FontSize40 u-Weight700 u-MarginTop100 u-xs-MarginTop30 u-MarginBottom0"
                >
                    <!--Annual Dev Conference 2017-->
                </h2>
                <h3
                    class="text-uppercase text-white u-Weight700 u-MarginTop0 u-MarginBottom50"
                >
                    <!--15 - 19 July, 2017, San
					Francisco, CA -->
                </h3>
            </div>

            <div class="col-md-5">
                <div
                    class="bg-black-transparent u-BorderRadius4 u-BoxShadow40 u-Padding10"
                >
                    <div
                        class="row u-PaddingTop30 u-PaddingLeft30 u-PaddingRight30"
                    >
                        <div class="col-md-12">
                            <form id="registerForm">
                                <?php echo e(csrf_field()); ?>

                                <input type="hidden" id="refCode" value="<?php echo e($ref_code ?? ""); ?>">

                                <h3 class="text-white u-MarginTop0">注册</h3>
                                <div class="form-group">
                                    <input
                                        class="form-control form-control--white"
                                        name="name"
                                        placeholder="姓名"
                                        type="text"
                                    />
                                </div>
                                <div class="form-group">
                                    <input
                                        class="form-control form-control--white"
                                        name="phone"
                                        placeholder="电话"
                                        type="text"
                                    />
                                </div>

                                <div class="form-group">
                                    <input
                                        class="form-control form-control--white"
                                        name="organization"
                                        placeholder="单位"
                                        type="text"
                                        type="text"
                                    />
                                </div>

                                <div class="form-group">
                                    <div class="row mb-2">
                                        <div class="col-xs-7">
                                            <input
                                                type="text"
                                                class="form-control"
                                                placeholder="验证码"
                                                name="otp"
                                            />
                                        </div>
                                        <div class="col-xs-5">
                                            <button
                                                class="btn btn-default btn-block"
                                                style = "padding: 0 10px"
						type="button"
                                                id="getOTPBtn"
                                            >获取验证码
                                            </button>
                                        </div>
                                    </div>
                                    <small class = "text-white hidden">注：如您收不到验证码，请联系组委会小敦：13007609501</small>
                                </div>

                                <div class="form-group">
                                    <button
                                        type="button"
                                        id="registerBtn"
                                        class="btn btn-primary btn-block"
                                    >
                                        注册
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
</section>
<!--hero section end-->

<?php $__env->stopSection(); ?>

<?php echo $__env->make('guest.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /www/wwwroot/pw.zzfexpo.gk18.cn/resources/views/guest/register.blade.php ENDPATH**/ ?>