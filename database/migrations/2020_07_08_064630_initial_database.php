<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitialDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('organization')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('password');
            $table->integer('role')->default(1);
            $table->unsignedBigInteger("city_id")->nullable();
            $table->string('address')->nullable();;
            $table->string('code')->nullable();
            $table->rememberToken();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('set null');

            $table->timestamps();
        });

        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('code')->unique()->nullable();
            $table->string('name');
            $table->string('email')->unique()->nullable();
            $table->string('organization')->nullable();
            $table->string('title')->nullable();
            $table->string('phone')->unique();
            $table->unsignedBigInteger("user_id")->nullable();
            $table->string("ref_code")->nullable();
            $table->string("ref_id")->nullable();
            $table->integer('ticket_status')->default(0);
            $table->integer('ticket_type')->default(0);
            $table->unsignedInteger("city")->nullable();
            $table->unsignedInteger("ticket_center")->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->longText('body');
            $table->longText('message')->nullable();
            $table->unsignedBigInteger("posted_by")->nullable();
            $table->integer('status')->default(0);
            $table->integer('score')->nullable();
            $table->integer('news_feed')->nullable();
            $table->integer('website')->nullable();
            $table->integer('media')->nullable();
            $table->timestamps();
            $table->foreign('posted_by')->references('id')->on('users')->onDelete('set null');
        });

        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->unsignedBigInteger("guest_id")->nullable();
            $table->unsignedBigInteger("user_id")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
        Schema::dropIfExists('posts');
        Schema::dropIfExists('customers');
        Schema::dropIfExists('users');
        Schema::dropIfExists('cities');
    }
}
